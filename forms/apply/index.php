<?php
session_start();
//error_reporting(0);
include('../processing-bits/functions.php');
include('../processing-bits/form-bits.php');

$errors = array();
$firstName = setValue('f-name');
$lastName = setValue('l-name');
$fullName = $firstName . ' ' . $lastName;
$email = setValue('email');
$phone = setValue('phone');
$date = setValue('date');
$position = setValue('position');
$whyWorkHere = setValue('why');
$github_link = setValue('github-link');
$codepen_link = setValue('codepen-link');
$other_info = setValue('other-info');
$read_hire_us = setValue('read-how-we-hire');
$read_how_work = setValue('read-how-we-work');
$ip = $_SERVER['REMOTE_ADDR'];
$success = FALSE;

$project_urls = setValueArray('project-url');
$project_details = setValueArray('project-details');



$uploadResume = $_FILES['resume']['name'];
$uploadCover = $_FILES['letter']['name'];



$tdHeading = 'width: 25%;';
$td = 'padding: 10px; font-family: sans-serif;';
$message = '
<table cellpadding="0" cellspacing="0" width="500px" border="1" style="margin: 0 auto; margin-bottom: 24px;">
	<tbody>
		<tr>
			<td colspan="2" style="' . $td . 'font-weight: bold; text-align: center;">Applicant Contact Details</td>
		</tr>
		<tr>
			<td style="' . $td . $tdHeading . '">Name</td>
			<td style="' . $td . '">' . $fullName . '</td>
		</tr>
		<tr>
			<td style="' . $td . $tdHeading . '">Email</td>
			<td style="' . $td . '">' . $email . '</td>
		</tr>
		<tr>
			<td style="' . $td . $tdHeading . '">Phone</td>
			<td style="' . $td . '">' . $phone . '</td>
		</tr>

	</tbody>
</table>
<table cellpadding="0" cellspacing="0" width="500px" border="1" style="margin: 0 auto; margin-bottom: 24px;">
	<tbody>
		<tr>
			<td colspan="2" style="' . $td . 'font-weight: bold; text-align: center;">Applicant Details</td>
		</tr>
		<tr>
			<td style="' . $td . $tdHeading . '">Position</td>
			<td style="' . $td . '">' . $position . '</td>
		</tr>
		<tr>
			<td style="' . $td . $tdHeading . '">Github link</td>
			<td style="' . $td . '"><a href="' . $github_link . '">'. $github_link . '</a></td>
		</tr>
		<tr>
			<td style="' . $td . $tdHeading . '">CodePen Link</td>
			<td style="' . $td . '"><a href="' . $codepen_link . '">'. $codepen_link . '</a></td>
		</tr>
		<tr>
			<td style="' . $td . $tdHeading . '">Anything Else You Want Us to Know</td>
			<td style="' . $td . '">' . $other_info . '</td>
		</tr>
	</tbody>
</table>
<table cellpadding="0" cellspacing="0" width="500px" border="1" style="margin: 0 auto; margin-bottom: 12px;">
	<tbody>
		<tr>
			<td colspan="2" style="' . $td . 'font-weight: bold; text-align: center;">Projects</td>
		</tr>

		';
		$x = 0;
		foreach ($project_urls as $project_url) {
			$message .= '
			<tr>
				<td style="' . $td . $tdHeading . '">Project URL</td>
				<td style="' . $td . '"><a href="' . $project_url . '">' . $project_url . '</a></td>
			</tr>
			<tr>
				<td style="' . $td . $tdHeading . '">What I Did</td>
				<td style="' . $td . '">' . $project_details[$x] . '</td>
			</tr>
			';

			$x++;
		}

	$message .= '</tbody>
</table>'
;

if(move_uploaded_file($_FILES['resume']['tmp_name'], $uploadResume)) {
	include '../processing-bits/smtp_mailgun_apply.php';

	//$mail->setFrom('customerservice@filingsmadeeasy.com', 'Corporate Tools LLC');
	//$mail->addAddress('customerservice@filingsmadeeasy.com', 'Corporate Tools LLC');
	//$mail->addAddress('web@llcagent.com');
	$mail->addReplyTo($email);
	// $mail->addAttachment($uploadResume, str_replace(" ", "_", $_POST['fullName']) .'.pdf');
	$mail->addAttachment($uploadResume);

	if(!empty($_FILES['letter'])) {

		if(move_uploaded_file($_FILES['letter']['tmp_name'], $uploadCover)) {
			// $mail->addAttachment($uploadCover, 'my_cover_letter.pdf');
			$mail->addAttachment($uploadCover);
		}
	}

	$mail->isHTML(true);
	$mail->Subject = '[Two Barrels Job Applicant] - '. $position .' - '. $fullName;
	$mail->Body = $message;


	if(!$mail->send()) {
		echo 'Error submitting your application.';
	} else {
		$success = TRUE;
		//header("Location: /thank-you");
		//exit;
	}

} else {
	echo 'No files uploaded!';
}


if ($success == TRUE) {
	//echo 'So far so good!';
	include '../processing-bits/autoresponder.php';
}

else {
	echo 'Error submitting your application.';
}
