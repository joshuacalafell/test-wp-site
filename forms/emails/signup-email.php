<?php error_reporting(0) ?>
<?php include 'email-style.php'; ?>
<html>
<head>
<table cellpadding="0" cellspacing="0"  align="center" style="margin: 0 auto; width: 100%; background: #fff;">
		<tr>
			<td>
				<center>
				<!--[if gte mso 9]>
					<table cellpadding="0" cellspacing="0" width="600px" style="width: 600px; padding: 0; margin: 0 auto;">
				<![endif]-->
				<!--[if !mso]><!-- -->
					<table cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px; width: 100%; padding: 10px 0; margin: 0 auto;">
				<!--<![endif]-->
						<tr>
							<td>
<table border="0" cellpadding="0" cellspacing="0" align="center" style="background: #fff;border: 1px solid #487f3c;" >  
	<tr> 
		<td align="right" style="<?=$firstColHead?>text-align: center; background: #214c24; color: #fff;">Form Name:</td> 
		<td style="<?=$firstColHead?> color: #fff;text-align: center; background: #214c24; color: #fff;"><a style="color:#fff;" href="<?=$originatingWebsite?>"><?=$originatingWebsite?></a> - <?php if($signup == 'ra'):?>Registered Agent<?php elseif($formType === 'private'): echo 'Private LLC'; elseif($formType === 'foreign'): echo 'Foreign Entity Registration'; elseif($signup == 'inc'):?>Incorporation<?php endif;?> Signup Form</td> 
	</tr> 
	<tr> 
		<td colspan="2" style="<?=$firstColHead?>">COMPANY INFORMATION: </td> 
	</tr>
	<tr> 
		<td align="right" style="<?=$firstCol?>">Entity Name:</td> 
		<td style="<?=$secondCol?>"><?=stripslashes($companyName);?></td> 
	</tr>
	<?php if( $formType == 'private' ): ?>
	<tr> 
		<td align="right" style="<?=$firstCol?>">Second Entity Name:</td> 
		<td style="<?=$secondCol?>"><?=stripslashes($secondCompanyName);?></td> 
	</tr>
	<?php endif; ?>
	<tr> 
		<td align="right" style="<?=$firstCol?>">State:</td> 
		<td style="<?=$secondCol?>"><?=$state_names;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Home State:</td> 
		<td style="<?=$secondCol?>"><?=$homeState;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Entity Type:</td> 
		<td style="<?=$secondCol?>"><?=$entityType;?></td> 
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">New or out of state company:</td>
		<td style="<?=$secondCol?>"><?=$newoldco;?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Use RA Address for Everything? :</td>
		<td style="<?=$secondCol?>"><?=$UseRAaddressAll;?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">List Member/Manager or Officer/Director on state docs?:</td>
		<td style="<?=$secondCol?>"><?=$anonymous;?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Principal address for State:</td>
		<td style="<?=$secondCol?>"><?=$principalAddressForState;?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Mailing Address for State:</td>
		<td style="<?=$secondCol?>"><?=$mailingAddressForState ; ?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Business Purpose:</td>
		<td style="<?=$secondCol?>"><?=$businessPurpose; ?></td>
	</tr>
	<?php if($entityType === 'LLC') {

		echo '
		<tr>
			<td align="right" style="' . $firstCol . '">LLC managed :</td>
			<td style="' . $secondCol . '">' . $memberOrmanager . '</td>
		</tr>';

		if($memberOrmanager === 'Member_Managed' || $memberOrmanager === 'Manager_Managed') {
			$i = 1;
			foreach($members as $name){
				echo '
				<tr>
					<td align="right" style="' . $firstCol . '">' . $i . ' Member</td>
					<td style="' . $secondCol . '">' . $name . '</td>
				</tr>
				<tr>
					<td align="right" style="' . $firstCol . '">Address</td>
					<td style="' . $secondCol . '">' . $RAAddress .'</td>
				</tr>';
				$i++;

			}
		} 

		if($memberOrmanager === 'Manager_Managed') {
			$i = 1;
			foreach($managers as $name){
				echo '
				<tr>
					<td align="right" style="' . $firstCol . '">' . $i . ' Manager</td>
					<td style="' . $secondCol . '">' . $name . '</td>
				</tr>
				<tr>
					<td align="right" style="' . $firstCol . '">Address</td>
					<td style="' . $secondCol . '">' . $RAAddress .'</td>
				</tr>';
				$i++;

			}
		} else {

		}
	}

	if($entityType === 'Corp' || $entityType === 'NonProfit_Corp') {
		echo '
		<tr>
			<td align="right" style="' . $firstCol . '">President :</td>
			<td style="' . $secondCol . '">' . stripslashes($president) . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">President Address:</td>
			<td style="' . $secondCol . '">' . stripslashes($presidentAddress) . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">Secretary :</td>
			<td style="' . $secondCol . '">' . stripslashes($secretary) . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">Secretary Address:</td>
			<td style="' . $secondCol . '">' . stripslashes($secretaryAddress) . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">Treasurer :</td>
			<td style="' . $secondCol . '">' . stripslashes($treasurer) . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">Treasurer Address:</td>
			<td style="' . $secondCol . '">' . stripslashes($treasurerAddress) . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">All Officers Are Directors :</td>
			<td style="' . $secondCol . '">' . $officerAreDirectors . '</td>
		</tr>';

		if($officers) {
			$i = 1;
			foreach($officers as $name){
				echo '
				<tr>
					<td align="right" style="' . $firstCol . '">' . $i . ' Officer</td>
					<td style="' . $secondCol . '">' . $name . '</td>
				</tr>
				<tr>
					<td align="right" style="' . $firstCol . '">Address</td>
					<td style="' . $secondCol . '">' . $RAAddress .'</td>
				</tr>';
				$i++;

			}
		}

		if($directors) {
			$i = 1;
			foreach($directors as $name){
				echo '
				<tr>
					<td align="right" style="' . $firstCol . '">' . $i . ' Director</td>
					<td style="' . $secondCol . '">' . $name . '</td>
				</tr>
				<tr>
					<td align="right" style="' . $firstCol . '">Address</td>
					<td style="' . $secondCol . '">' . $RAAddress .'</td>
				</tr>';
				$i++;
			}
		}

		echo '
		<tr>
			<td align="right" style="' . $firstCol . '">Shares Authorized:</td>
			<td style="' . $secondCol . '">' . $sharesAuthorized . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">Par Value:</td>
			<td style="' . $secondCol . '">' . $parValue . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">Number Shares Issued:</td>
			<td style="' . $secondCol . '">' . $numberSharesIssued . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">All Common Shares?:</td>
			<td style="' . $secondCol . '">' . $commonShares . '</td>
		</tr>
		<tr>
			<td align="right" style="' . $firstCol . '">Type of Shares:</td>
			<td style="' . $secondCol . '">' . $typeCommonShares . '</td>
		</tr>';
	}
	?>

	<tr> 
		<td colspan="2" style="<?=$firstColHead?>">CONTACT INFORMATION: </td> 
	</tr>  
	<tr> 
		<td align="right" style="<?=$firstCol?>">Mailing Address:</td> 
		<td style="<?=$secondCol?>"><?=$mailAddress;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Mailing Address City:</td> 
		<td style="<?=$secondCol?>"><?=$mailingCity;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Mailing Address State:</td> 
		<td style="<?=$secondCol?>"><?=$mailingState;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Mailing Address Zip Code:</td> 
		<td style="<?=$secondCol?>"><?=$mailingZipcode;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Mailing Address Country:</td> 
		<td style="<?=$secondCol?>"><?=$mailingCountry;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Contact Name:</td> 
		<td style="<?=$secondCol?>"><?=$fullName?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Phone Number:</td> 
		<td style="<?=$secondCol?>"><?=$phone;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Fax Number:</td> 
		<td style="<?=$secondCol?>"><?=$fax;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Alternate Phone Number:</td> 
		<td style="<?=$secondCol?>"><?=$phoneAlt;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Email Address:</td> 
		<td style="<?=$secondCol?>"><?=$email;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Secondary Email Address:</td> 
		<td style="<?=$secondCol?>"><?=$emailAlt;?></td> 
	</tr> 
	
	<tr> 
		<td colspan="2" style="<?=$firstColHead?>">OPTIONAL ITEMS: </td> 
	</tr> 
	<tr>
		<td align="right" style="<?=$firstCol?>">Corp Book:</td>
		<td style="<?=$secondCol?>"><?=$corpBook;?> &ndash; $<?=$CorpBookPrice?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Corp Seal:</td>
		<td style="<?=$secondCol?>"><?=$corpSeal;?> &ndash; $<?=$CorpSealPrice?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Tax ID:</td>
		<td style="<?=$secondCol?>"><?=$TaxId;?> &ndash; $<?=$TaxIdPrice?> <?php if($TaxIdType):?>(<?=$TaxIdType?>)<?php endif; ?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Certified Copy:</td>
		<td style="<?=$secondCol?>"><?=$CCopy;?> &ndash; $<?=$CCopyprice?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Good Standing:</td>
		<td style="<?=$secondCol?>"><?=$COG;?> &ndash; $<?=$COGPrice?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">S-Corp:</td>
		<td style="<?=$secondCol?>"><?=$CCopy;?> &ndash; $<?=$sCorpPrice?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Apostille:</td>
		<td style="<?=$secondCol?>"><?=$COG;?> &ndash; $<?=$Apostileprice?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Annual Report Compliance:</td>
		<td style="<?=$secondCol?>"><?=$AR;?> &ndash; $<?=$ARprice?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Mail Forwarding:</td>
		<td style="<?=$secondCol?>">no &ndash; N/A<td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Call Forwarding:</td>
		<td style="<?=$secondCol?>">no &ndash; N/A</td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">UBI Number:</td>
		<td style="<?=$secondCol?>"><?=$ubi?> &ndash; <?=$ubiPrice?></td>
	</tr>

	<tr> 
		<td colspan="2" style="<?=$firstColHead?>">SIGNUP INFORMATION: </td> 
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Notes:</td>
		<td style="<?=$secondCol?>"><?=$specialNotes;?></td>
	</tr>
	<tr> 
		<td align="right" style="<?=$firstCol?>">Terms And conditions:</td> 
		<td style="<?=$secondCol?>"><?=$readService;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Registered Agent Yearly Fee:</td> 
		<td style="<?=$secondCol?>"><?=$regAgentFee;?></td> 
	</tr> 
	<tr>
		<td align="right" style="<?=$firstCol?>">Incorporation Fee:</td>
		<td style="<?=$secondCol?>"><?=$incorpFee;?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">State Fee:</td>
		<td style="<?=$secondCol?>"><?=$state_fees;?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Expedite Fee:</td>
		<td style="<?=$secondCol?>"><?=$ExpProcessingFee;?></td>
	</tr>
	<tr>
		<td align="right" style="<?=$firstCol?>">Expedite Processing Level:</td>
		<td style="<?=$secondCol?>"><?=$ExpProcessingLevel;?></td>
	</tr>
	<tr> 
		<td align="right" style="<?=$firstCol?>">Number of years of service:</td> 
		<td style="<?=$secondCol?>"><?=$numYears;?></td> 
	</tr>
	<tr> 
		<td align="right" style="<?=$firstCol?>">Purchase Order Number:</td> 
		<td style="<?=$secondCol?>"><?=$purchaseOrderNumber; ?></td> 
	</tr>
	<tr> 
		<td align="right" style="<?=$firstCol?>">Changing registered agent? :</td> 
		<td style="<?=$secondCol?>"><?=$changeAgent;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Notify Attorney of Law Suit? :</td> 
		<td style="<?=$secondCol?>"><?=$notifyAttorney;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Attorney's Address:</td> 
		<td style="<?=$secondCol?>"><?=$attorneyEmail;?></td> 
	</tr>

	<tr> 
		<td colspan="2" style="<?=$firstColHead?>">PAYMENT INFORMATION: </td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">IP Address:</td> 
		<td style="<?=$secondCol?>"><?=$ip;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Total Payment : </td> 
		<td style="<?=$secondCol?>"><?=$itemCost;?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">#1 Response Code (approved=1): </td> 
		<td style="<?=$secondCol?>"><?=$response_array[0] ; ?></td> 
	</tr> 
	<tr> 
		<td align="right" style="<?=$firstCol?>">Special Addons/D.C. : </td> 
		<td style="<?=$secondCol?>"><?=$dis ; ?></td> 
	</tr> 
</table> 

							</td>
				        </tr>
					</table>
				</center>
			</td>
		</tr>
	</table>
</body>
</html>