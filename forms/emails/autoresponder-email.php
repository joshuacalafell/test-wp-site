<?php include 'email-style.php'; ?>
<?php
  $ar_email = '
  <body style="Margin: 30px auto;display: block;max-width: 600px;width: 90%;">
  <img style="text-align: center;display: block;margin: 16px auto 2em;width: 110px;" src="https://twobarrels.com/twob-2.png">
  <p style="font-family: sans-serif;line-height: 40px;font-size: 32px;Margin: 0 0 16px;color: #272727;letter-spacing: 1px;font-weight: 400;">Thank you for taking the time to apply.</p>
  <p style="<?=$paragraph?>">We wanted you to know we\'ve received your application. We review all resumes and cover letters that we receive and will reach out if we should require further information. If you don\'t hear back, that means we have either moved forward with other candidates or paused the hiring process for this position.
Thank you, again, for your interest in Two Barrels. We appreciate the time invested in this application and look forward to reviewing it. </p>
  <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">Human Resources</p>
  <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">Two Barrels LLC</p>
  <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">401 W. 1st Ave</p>
  <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">Spokane, WA 99201</p>
  <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">hr@twobarrels.com</p>
    <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">(509) 315-1149</p>
  <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">twobarrels.com</p>
    <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">https://www.twobarrels.com/how-to-get-a-job-here/</p>
      <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">https://www.twobarrels.com/how-we-work/</p>
        <p style="font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;<?=$paragraph?>">https://www.twobarrels.com/who-we-are/</p>
  </body>
  ';
?>
