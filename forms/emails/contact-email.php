<?php include 'email-style.php'; ?>
<html>
<head>
<table cellpadding="0" cellspacing="0"  align="center" style="margin: 0 auto; width: 100%; background: #fff;">
		<tr>
			<td>
				<center>
				<!--[if gte mso 9]>
					<table cellpadding="0" cellspacing="0" width="600px" style="width: 600px; padding: 0; margin: 0 auto;">
				<![endif]-->
				<!--[if !mso]><!-- -->
					<table cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px; width: 100%; padding: 10px 0; margin: 0 auto;">
				<!--<![endif]-->
						<tr>
							<td>
								<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; background: #fff;border-left:1px solid #02547D;">
									<tr valign="top">
										<td style="width: 100%;">
											<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
												<tr style="<?=$tr?>background: #fff;">
													<td colspan="2" style="<?=$firstCol?>text-align: center; background: #02547D; color: #fff; font-weight:400;">New Contact Inquiry For <span style="font-weight:700; font-size:20px; margin:0 .5em;"><?php echo WEBSITE_URL;?></span></td>
												</tr>
												<tr style="<?=$tr?>">
													<td style="<?=$firstCol?>"><span style="<?=$bold?>">Name:</span></td>
													<td style="<?=$secondCol?>"><?=$fullname?></td>
												</tr>
												<tr style="<?=$tr?>">
													<td style="<?=$firstCol?>"><span style="<?=$bold?>">Message:</span></td>
													<td style="<?=$secondCol?>"><?=$message?></td>
												</tr>

												<tr style="<?=$tr?>">
													<td style="<?=$firstCol?>"><span style="<?=$bold?>">Email:</span></td>
													<td style="<?=$secondCol?>"><a href="mailto:<?=$email?>"><?=$email?></a></td>
												</tr>
												<tr style="<?=$tr?>">
													<td style="<?=$firstCol?>"><span style="<?=$bold?>">IP:</span></td>
													<td style="<?=$secondCol?>"><?=$ip?></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
				        </tr>
					</table>
				</center>
			</td>
		</tr>
	</table>
</body>
</html>
