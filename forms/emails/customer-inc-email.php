<?php include 'email-style.php'; ?>
<?php include 'email-top.php'; ?>


<p style="<?=$paragraph?>">Thank you for letting us help you make the important step of incorporating your new corporation or forming your new company. We appreciate your business, and rest assured, we don't take the responsibility lightly to help you along your way. We try to be the fastest incorporation service you could possibly find.</p>

<p style="<?=$paragraph?>">The famous question I always hear now is..."I just signed up on your website...Now what?" </p>
<p style="<?=$paragraph?><?=$bold?>">Well here's the rundown:</p>
<ol>
	<li style="<?=$li?><?=$liNumber?>">We'll get started with the filing right away.</li>
	<li style="<?=$li?><?=$liNumber?>">NOTE: If you have picked any of the optional services we cannot process those specific add-ons until you login to your online account and complete the necessary additional information.</li>
	<li style="<?=$li?><?=$liNumber?>">When we get verification back from the state, we'll get the documents back to you. We electronically check for your filing status every hour. When we get you your finalized public documents and internal documents, you should have what you need to open a bank account.</li>
	<li style="<?=$li?><?=$liNumber?>">After the company is formed, you will need to:
		<ul>
			<li style="<?=$li?>">Get a federal tax ID number from the IRS. (you might have hired us to do this part)</li>
			<li style="<?=$li?>">Check to see if you need a specific license locally and at the state level for your type of business, and if so, obtain one.</li>
			<li style="<?=$li?>">Check to see if you need to register with the state department of revenue, and if so, register.</li>
		</ul>
	</li>
	<li style="<?=$li?><?=$liNumber?>">When the all the filings you hired us to do are complete, we will upload the documents and update your information in your online account. This will set your state filing dates, making the auto-reminders we send out accurate.</li>
</ol>
<p style="<?=$paragraph?>">Thank you for your business. We appreciate the opportunity to grow alongside yours and wish you the best success in your new venture. </p>


<?php include 'email-bottom.php'; ?>