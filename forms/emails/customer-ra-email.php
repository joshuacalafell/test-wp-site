<?php include 'email-style.php'; ?>
<?php include 'email-top.php'; ?>

<p style="<?=$paragraph?>">Thank you for signing up for registered agent service with Washington Commercial Registered Agent LLC! We look forward to working with your company and continuing to earn your business.</p>
<p style="<?=$paragraph?>">You can now access your online customer account, from which you can add any business filing you need now and in the future. You account comes loaded with pre-signed documents and easy-to-use filing instructions, should you want to make the filings yourself.</p>
<p style="<?=$paragraph?>">We look forward to providing the highest-quality year-round compliance monitoring and tracking for your business. Please feel free to contact us at any time if you have any questions regarding our service.</p>

<?php include 'email-bottom.php'; ?>