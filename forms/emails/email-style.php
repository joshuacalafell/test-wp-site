<?php
	$paragraph = 'font-family: sans-serif;line-height: 24px; font-size:16px;margin:0 0 16px;';
	$h3 = 'font-family: sans-serif; font-size: 20px; margin: 25px 0 7px;';
	$li = 'font-family: sans-serif;line-height: 24px; font-size:16px;list-style: square;margin-bottom: 5px;';
	$h4 = 'font-family: sans-serif; font-size: 18px;';
	$firstColHead = 'padding: 10px 5px;font-family:Arial;font-size:14px;font-weight:bold;color:White;background-color:#272727;';
	$longText = 'word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto;';
	$bold = 'font-weight: bold;';
	$liNumber = 'list-style: decimal;';
	$secondColHead = 'padding: 8px;font-family:Arial;font-size:14px;font-weight:bold;color:White;background-color:#272727;';
	$secondCol = 'padding: 8px;font-family:Arial;font-size:14px;width: 80%;vertical-align: top; color:#4d4d4d;border-bottom: 1px solid #02547D;border-right:1px solid #02547D;';
	$firstColHead = 'padding: 8px;font-family:Arial;font-size:14px;font-weight:bold;color:White;background-color:#272727;';
	$firstCol = 'width: 20%; padding: 8px;font-family:Arial;font-size:14px;font-weight:bold;color:#4d4d4d;vertical-align: top; border-bottom: 1px solid #02547D;border-right:1px solid #02547D;';
	$tr = 'width: 100%; border-bottom:1px solid black;';
	$footer = 'font-family: sans-serif;line-height: 15px;font-size: 14px;Margin: 0 0 5px;color: #6f6f6f;letter-spacing: 1px;';
