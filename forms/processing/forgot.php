<?php
session_start();
error_reporting(0);
include_once 'inc/website-info.php';

$originatingWebsite = 'www.' . WEBSITE_URL;
$cleanUsername = filter_input(INPUT_POST, "username", FILTER_VALIDATE_EMAIL);
$login_credentials = array(
	'email' => $cleanUsername,
	'url'	=> $originatingWebsite
);

$body = json_encode($login_credentials);
$url = 'https://api.agentprocessing.com/v1/session/forgot';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'Content-Length: ' . strlen($body)
]);
$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
$result = json_decode(curl_exec($ch), TRUE);
if ($result['success'] == TRUE) {
    $_SESSION['forgotSuccess'] = "We sent you an email with instructions to reset you password. If you don't receive an email in the next 15 minutes, you will need to call us to reset your password. Call us at: " . WEBSITE_PHONE;
}
header("Location: " . FORGOT_PASSWORD_PAGE);
exit;