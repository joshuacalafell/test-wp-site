<?php
session_start();
error_reporting(0);

include_once 'inc/website-info.php';

$originatingWebsite = 'www.' . WEBSITE_URL;

$login_credentials = array(
	'username' => $_POST['username'],
	'password' => $_POST['password'],
	'url'      => $originatingWebsite
);

$url = 'https://api.agentprocessing.com/v1/session';
$curlPost = curl_init();
curl_setopt($curlPost, CURLOPT_URL, $url);
curl_setopt($curlPost, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlPost, CURLOPT_POST, 1);
curl_setopt($curlPost, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
curl_setopt($curlPost, CURLOPT_POSTFIELDS, $login_credentials);
$response = curl_exec($curlPost);
$httpcode = curl_getinfo($curlPost, CURLINFO_HTTP_CODE);
curl_close($curlPost);

$result = json_decode($response, true);

if ($httpcode == 400) {
	$_SESSION['loginErrors'] = $result['message'];
	header("Location: " . LOGIN_PAGE);
	exit;
} else if ($httpcode == 200) {
	$account_type = $result['type'];
	if ($account_type === 'corptools') {
		header("Location: " . $result['login_url']);
		exit;
	}
} else {
	$_SESSION['loginErrors'] = $result['message'];
	header("Location: " . LOGIN_PAGE);
	exit;
}
?>
