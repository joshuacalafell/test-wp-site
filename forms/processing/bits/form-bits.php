<?php 

function selectedValueVar($var,$get) {
	
	if(isset($_GET[$get]) ) {
		$get = $_GET[$get];
		if($get === $var) {
			return "selected";
		}
	}
	
	if($var === '' && !isset($_GET[$get])) {
		return "selected";
	}
}

function selectedValue($var,$get) {
	
	if(isset($_GET[$get])){
		$get = $_GET[$get];
		if($get === $var) {
			echo "selected";
		}
	}

	if($var === '' && !isset($_GET[$get])) {
		echo "selected";
	}
}
   


function addPerson($person, $validator){
	$i = 0;
	$personTitle = ucfirst($person);
	global $form;
	echo'
	<div class="' . $person . '-' . $validator . ' person-container form-group half">'; 
		if (isset($_SESSION[$person]) && !empty($_SESSION[$person]) ) {
			$thePeople = $_SESSION[$person];

			
			foreach ($thePeople as $thePerson) {

				echo
				'<div class="person">
					<label>' . $personTitle . ':</label>
					<i class="remove-person icon-times"></i>
					<div class="row">
						<div class="form-group half">
							<input data-validator="' . $validator . '" type="text" name="' . $person . '[' . $i . '][first_name]" value="' . $thePerson['first_name'] . '" placeholder="First">
							<small class="form-error">' . $personTitle . ' is required</small>
						</div>
						<div class="form-group half">
							<input data-validator="' . $validator . '" type="text" name="' . $person . '[' . $i . '][last_name]" value="' . $thePerson['last_name'] . '" placeholder="Last">
							<small class="form-error">' . $personTitle . ' is required</small>
						</div>
					</div>';

				if($form === 'foreign') {
					echo'
					<select class="person-address-type" name="' . $person . '-address[]">
						<option value="Registered Agent">Registered Agent Address</option>
						<option value="Mailing Address">Mailing Address</option>
						<option value="Specify Address">Specify Address</option>
					</select>
					<div class="row">
						<div class="person-address ' . $person . '[' . $i . '][address][address_country]">
							<div class="form-group">
								<label>Country:</label>
								<select name="' . $person . '[' . $i . '][address][address_country]">';
									echo countryList($person . '-country-state'); echo '
								</select>
								<small class="form-error">Country is required</small>
							</div>
							<div class="form-group">
								<label>Street Address:</label>
								<input name="' . $person . '[' . $i . '][address][address_line1] type="text">
								<small class="form-error">Street Address is required</small>
							</div>
							<div class="form-group">
								<label>City:</label>
								<input name="' . $person . '[' . $i . '][address][address_city] type="text">
								<small class="form-error">City is required</small>
							</div>
							<div class="form-group">
								<label>State<span class="non-usa">/Province</span>:</label>
								<div class="usa">
									<select name="' . $person . '[' . $i . '][address][address_region]>';
										echo stateList($person . '-state-state'); echo'
									</select>
									<small class="">province</small>
								</div>
								<div class="non-usa">
									<input name="' . $person . '[' . $i . '][address][address_region]" type="text">
									<small class="">province</small>
								</div>
							</div>
							<div class="form-group">
								<label><span class="usa">Zip</span><span class="non-usa">Postal</span> Code:</label>
								<input name="' . $person . '[' . $i . '][address][address_code]" type="text" pattern="[0-9]*">
								<small class="form-error"><span class="usa">Zip</span><span class="non-usa">Postal</span> code required</small>
							</div>
						</div>
					</div>';
				}

				echo '</div>';

				$i++;
			}
		} else {

			echo
			'<div class="person">
				<label>' . $personTitle . ':</label>
				<i class="remove-person icon-times"></i>
				<div class="row">
					<div class="form-group half">
						<input data-validator="' . $validator . '" type="text" name="' . $person . '[' . $i . '][first_name]" placeholder="First">
						<small class="form-error">' . $personTitle . ' is required</small>
					</div>
					<div class="form-group half">
						<input data-validator="' . $validator . '" type="text" name="' . $person . '[' . $i . '][last_name]" placeholder="Last">
						<small class="form-error">' . $personTitle . ' is required</small>
					</div>
				</div>';

			if($form === 'foreign') {
				echo'
				<select class="person-address-type" name="' . $person . '-address[]">
					<option value="Registered Agent">Registered Agent Address</option>
					<option value="Mailing Address">Mailing Address</option>
					<option value="Specify Address">Specify Address</option>
				</select>
				<div class="row">
					<div class="person-address ' . $person . '-address-state">
						<div class="form-group">
							<label>Country:</label>
							<select class="person-country" name="' .  $person . '[' . $i . '][address][address_country]">';
								echo countryList($person . '-country-state'); echo '
							</select>
							<small class="form-error">Country is required</small>
						</div>
						<div class="form-group">
							<label>Street Address:</label>
							<input name="' .  $person . '[' . $i . '][address][address_line1]" type="text">
							<small class="form-error">Street Address is required</small>
						</div>
						<div class="form-group">
							<label>City:</label>
							<input name="' .  $person . '[' . $i . '][address][address_city]" type="text">
							<small class="form-error">City is required</small>
						</div>
						<div class="form-group">
							<label>State<span class="non-usa">/Province</span>:</label>
							<div class="usa">
								<select class="person-state" name="' .  $person . '[' . $i . '][address][address_state]">';
									echo stateList($person . '-state-state'); echo'
								</select>
								<small class="">province</small>
							</div>
							<div class="non-usa">
								<input class="person-providence" name="' . $person . '[' . $i . '][address][address_providence]" type="text">
								<small class="">province</small>
							</div>
						</div>
						<div class="form-group">
							<label><span class="usa">Zip</span><span class="non-usa">Postal</span> Code:</label>
							<input class="person-code" name="' .  $person . '[' . $i . '][address][address_code]" type="text" pattern="[0-9]*">
							<small class="form-error"><span class="usa">Zip</span><span class="non-usa">Postal</span> code required</small>
						</div>
					</div>
				</div>';
			}

			echo '</div>';
		}

		echo
		'<div class="add-person">
			<span tabindex="0">Additional ' . $personTitle . '</span>
		</div>
	</div>';
}

$statesTerritoriesArray = array( 
	'AK' => 'Alaska',
	'AL' => 'Alabama',
	'AR' => 'Arkansas',
	'AZ' => 'Arizona',
	'CA' => 'California',
	'CO' => 'Colorado',
	'CT' => 'Connecticut',
	'DC' => 'District of Columbia',
	'DE' => 'Delaware',
	'FL' => 'Florida',
	'GA' => 'Georgia',
	'HI' => 'Hawaii',
	'IA' => 'Iowa',
	'ID' => 'Idaho',
	'IL' => 'Illinois',
	'IN' => 'Indiana',
	'KS' => 'Kansas',
	'KY' => 'Kentucky',
	'LA' => 'Louisiana',
	'MA' => 'Massachusetts',    
	'MD' => 'Maryland',
	'ME' => 'Maine',
	'MI' => 'Michigan',
	'MN' => 'Minnesota',
	'MO' => 'Missouri',
	'MS' => 'Mississippi',
	'MT' => 'Montana',
	'NC' => 'North Carolina',
	'ND' => 'North Dakota',
	'NE' => 'Nebraska',
	'NH' => 'New Hampshire',
	'NJ' => 'New Jersey',
	'NM' => 'New Mexico',
	'NV' => 'Nevada',
	'NY' => 'New York',
	'OH' => 'Ohio',
	'OK' => 'Oklahoma',
	'OR' => 'Oregon',
	'PA' => 'Pennsylvania',
	'RI' => 'Rhode Island',
	'SC' => 'South Carolina',
	'SD' => 'South Dakota',
	'TN' => 'Tennessee',
	'TX' => 'Texas',
	'UT' => 'Utah',
	'VA' => 'Virginia',
	'VT' => 'Vermont',
	'WA' => 'Washington',
	'WI' => 'Wisconsin',
	'WV' => 'West Virginia',
	'WY' => 'Wyoming',

	'US Military' => array(
		'AA' => 'Armed Forces Americas',
		'AE' => 'Armed Forces Europe',
		'AP' => 'Armed Forces Pacific'
		),

	'US Territories' => array(
		'AS' => 'American Samoa',
		'GU' => 'Guam',
		'MP' => 'Northern Mariana Islands',
		'PR' => 'Puerto Rico',
		'VI' => 'US Virgin Islands'
		)
	);

$countryArray = array(
	'AF' => 'Afghanistan',
	'AX' => 'Åland',
	'AL' => 'Albania',
	'DZ' => 'Algeria',
	'AS' => 'American Samoa',
	'AD' => 'Andorra',
	'AO' => 'Angola',
	'AI' => 'Anguilla',
	'AQ' => 'Antarctica',
	'AG' => 'Antigua and Barbuda',
	'AR' => 'Argentina',
	'AM' => 'Armenia',
	'AW' => 'Aruba',
	'AU' => 'Australia',
	'AT' => 'Austria',
	'AZ' => 'Azerbaijan',
	'BS' => 'Bahamas',
	'BH' => 'Bahrain',
	'BD' => 'Bangladesh',
	'BB' => 'Barbados',
	'BY' => 'Belarus',
	'BE' => 'Belgium',
	'BZ' => 'Belize',
	'BJ' => 'Benin',
	'BM' => 'Bermuda',
	'BT' => 'Bhutan',
	'BO' => 'Bolivia',
	'BQ' => 'Bonaire',
	'BA' => 'Bosnia and Herzegovina',
	'BW' => 'Botswana',
	'BV' => 'Bouvet Island',
	'BR' => 'Brazil',
	'IO' => 'British Indian Ocean Territory',
	'VG' => 'British Virgin Islands',
	'BN' => 'Brunei',
	'BG' => 'Bulgaria',
	'BF' => 'Burkina Faso',
	'BI' => 'Burundi',
	'KH' => 'Cambodia',
	'CM' => 'Cameroon',
	'CA' => 'Canada',
	'CV' => 'Cape Verde',
	'KY' => 'Cayman Islands',
	'CF' => 'Central African Republic',
	'TD' => 'Chad',
	'CL' => 'Chile',
	'CN' => 'China',
	'CX' => 'Christmas Island',
	'CC' => 'Cocos [Keeling] Islands',
	'CO' => 'Colombia',
	'KM' => 'Comoros',
	'CK' => 'Cook Islands',
	'CR' => 'Costa Rica',
	'HR' => 'Croatia',
	'CU' => 'Cuba',
	'CW' => 'Curacao',
	'CY' => 'Cyprus',
	'CZ' => 'Czech Republic',
	'CD' => 'Democratic Republic of the Congo',
	'DK' => 'Denmark',
	'DJ' => 'Djibouti',
	'DM' => 'Dominica',
	'DO' => 'Dominican Republic',
	'TL' => 'East Timor',
	'EC' => 'Ecuador',
	'EG' => 'Egypt',
	'SV' => 'El Salvador',
	'GQ' => 'Equatorial Guinea',
	'ER' => 'Eritrea',
	'EE' => 'Estonia',
	'ET' => 'Ethiopia',
	'FK' => 'Falkland Islands',
	'FO' => 'Faroe Islands',
	'FJ' => 'Fiji',
	'FI' => 'Finland',
	'FR' => 'France',
	'GF' => 'French Guiana',
	'PF' => 'French Polynesia',
	'TF' => 'French Southern Territories',
	'GA' => 'Gabon',
	'GM' => 'Gambia',
	'GE' => 'Georgia',
	'DE' => 'Germany',
	'GH' => 'Ghana',
	'GI' => 'Gibraltar',
	'GR' => 'Greece',
	'GL' => 'Greenland',
	'GD' => 'Grenada',
	'GP' => 'Guadeloupe',
	'GU' => 'Guam',
	'GT' => 'Guatemala',
	'GG' => 'Guernsey',
	'GN' => 'Guinea',
	'GW' => 'Guinea-Bissau',
	'GY' => 'Guyana',
	'HT' => 'Haiti',
	'HM' => 'Heard Island and McDonald Islands',
	'HN' => 'Honduras',
	'HK' => 'Hong Kong',
	'HU' => 'Hungary',
	'IS' => 'Iceland',
	'IN' => 'India',
	'ID' => 'Indonesia',
	'IR' => 'Iran',
	'IQ' => 'Iraq',
	'IE' => 'Ireland',
	'IM' => 'Isle of Man',
	'IL' => 'Israel',
	'IT' => 'Italy',
	'CI' => 'Ivory Coast',
	'JM' => 'Jamaica',
	'JP' => 'Japan',
	'JE' => 'Jersey',
	'JO' => 'Jordan',
	'KZ' => 'Kazakhstan',
	'KE' => 'Kenya',
	'KI' => 'Kiribati',
	'XK' => 'Kosovo',
	'KW' => 'Kuwait',
	'KG' => 'Kyrgyzstan',
	'LA' => 'Laos',
	'LV' => 'Latvia',
	'LB' => 'Lebanon',
	'LS' => 'Lesotho',
	'LR' => 'Liberia',
	'LY' => 'Libya',
	'LI' => 'Liechtenstein',
	'LT' => 'Lithuania',
	'LU' => 'Luxembourg',
	'MO' => 'Macao',
	'MK' => 'Macedonia',
	'MG' => 'Madagascar',
	'MW' => 'Malawi',
	'MY' => 'Malaysia',
	'MV' => 'Maldives',
	'ML' => 'Mali',
	'MT' => 'Malta',
	'MH' => 'Marshall Islands',
	'MQ' => 'Martinique',
	'MR' => 'Mauritania',
	'MU' => 'Mauritius',
	'YT' => 'Mayotte',
	'MX' => 'Mexico',
	'FM' => 'Micronesia',
	'MD' => 'Moldova',
	'MC' => 'Monaco',
	'MN' => 'Mongolia',
	'ME' => 'Montenegro',
	'MS' => 'Montserrat',
	'MA' => 'Morocco',
	'MZ' => 'Mozambique',
	'MM' => 'Myanmar [Burma]',
	'NA' => 'Namibia',
	'NR' => 'Nauru',
	'NP' => 'Nepal',
	'NL' => 'Netherlands',
	'NC' => 'New Caledonia',
	'NZ' => 'New Zealand',
	'NI' => 'Nicaragua',
	'NE' => 'Niger',
	'NG' => 'Nigeria',
	'NU' => 'Niue',
	'NF' => 'Norfolk Island',
	'KP' => 'North Korea',
	'MP' => 'Northern Mariana Islands',
	'NO' => 'Norway',
	'OM' => 'Oman',
	'PK' => 'Pakistan',
	'PW' => 'Palau',
	'PS' => 'Palestine',
	'PA' => 'Panama',
	'PG' => 'Papua New Guinea',
	'PY' => 'Paraguay',
	'PE' => 'Peru',
	'PH' => 'Philippines',
	'PN' => 'Pitcairn Islands',
	'PL' => 'Poland',
	'PT' => 'Portugal',
	'PR' => 'Puerto Rico',
	'QA' => 'Qatar',
	'CG' => 'Republic of the Congo',
	'RE' => 'Réunion',
	'RO' => 'Romania',
	'RU' => 'Russia',
	'RW' => 'Rwanda',
	'BL' => 'Saint Barthélemy',
	'SH' => 'Saint Helena',
	'KN' => 'Saint Kitts and Nevis',
	'LC' => 'Saint Lucia',
	'MF' => 'Saint Martin',
	'PM' => 'Saint Pierre and Miquelon',
	'VC' => 'Saint Vincent and the Grenadines',
	'WS' => 'Samoa',
	'SM' => 'San Marino',
	'ST' => 'São Tomé and Príncipe',
	'SA' => 'Saudi Arabia',
	'SN' => 'Senegal',
	'RS' => 'Serbia',
	'SC' => 'Seychelles',
	'SL' => 'Sierra Leone',
	'SG' => 'Singapore',
	'SX' => 'Sint Maarten',
	'SK' => 'Slovakia',
	'SI' => 'Slovenia',
	'SB' => 'Solomon Islands',
	'SO' => 'Somalia',
	'ZA' => 'South Africa',
	'GS' => 'South Georgia and the South Sandwich Islands',
	'KR' => 'South Korea',
	'SS' => 'South Sudan',
	'ES' => 'Spain',
	'LK' => 'Sri Lanka',
	'SD' => 'Sudan',
	'SR' => 'Suriname',
	'SJ' => 'Svalbard and Jan Mayen',
	'SZ' => 'Swaziland',
	'SE' => 'Sweden',
	'CH' => 'Switzerland',
	'SY' => 'Syria',
	'TW' => 'Taiwan',
	'TJ' => 'Tajikistan',
	'TZ' => 'Tanzania',
	'TH' => 'Thailand',
	'TG' => 'Togo',
	'TK' => 'Tokelau',
	'TO' => 'Tonga',
	'TT' => 'Trinidad and Tobago',
	'TN' => 'Tunisia',
	'TR' => 'Turkey',
	'TM' => 'Turkmenistan',
	'TC' => 'Turks and Caicos Islands',
	'TV' => 'Tuvalu',
	'UM' => 'U.S. Minor Outlying Islands',
	'VI' => 'U.S. Virgin Islands',
	'UG' => 'Uganda',
	'UA' => 'Ukraine',
	'AE' => 'United Arab Emirates',
	'GB' => 'United Kingdom',
	'US' => 'United States',
	'UY' => 'Uruguay',
	'UZ' => 'Uzbekistan',
	'VU' => 'Vanuatu',
	'VA' => 'Vatican City',
	'VE' => 'Venezuela',
	'VN' => 'Vietnam',
	'WF' => 'Wallis and Futuna',
	'EH' => 'Western Sahara',
	'YE' => 'Yemen',
	'ZM' => 'Zambia',
	'ZW' => 'Zimbabwe'
	);

function countryList($fieldName){
	global $countryArray;
	$countryOptions = '';
	foreach ($countryArray as $countryCode => $countryName) {
		$countryOptions .= '<option ' . sessionSelectReturn($fieldName, $countryCode); if($countryName == 'United States'): $countryOptions .= 'selected'; endif; $countryOptions .= ' value="' . $countryCode . '">' . $countryName . '</option>';
	}
	unset($_SESSION[$fieldName]);
	return $countryOptions;
}

function stateList($fieldName){
	global $statesTerritoriesArray;
	$stateList = '<option selected disabled value="">Select State</option>';
	foreach ($statesTerritoriesArray as $key => $value) {
		
		if (is_array($value)) {
			$stateList .= '<optgroup label="'. $key .'">';
			
			foreach ($value as $k2 => $v2) {
				$stateList .= '<option ' . sessionSelectReturn($fieldName, $k2) . ' value="'. $k2 .'">'. $v2 .'</option>';
			}
			
			$stateList .= '</optgroup>';
			
		} else {
			$stateList .= '<option ' . sessionSelectReturn($fieldName, $key) . ' value="'. $key .'">'. $value .'</option>';
		}
	}
	unset($_SESSION[$fieldName]);
	return $stateList;
}

function foreignStateList($fieldName){
	global $statesTerritoriesArray;
	$foreignStateList = '<option selected disabled value="">Select Home State</option>';
	foreach ($statesTerritoriesArray as $key => $value) {
		
		if (is_array($value)) {
			
		} elseif($key !== 'ID') {
			$foreignStateList .= '<option ' . sessionSelectReturn($fieldName, $key) . ' value="'. $key .'">'. $value .'</option>';
		}
	}
	unset($_SESSION[$fieldName]);
	return $foreignStateList;
}