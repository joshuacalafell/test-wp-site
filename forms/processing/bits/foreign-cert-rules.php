<?php 
/**
 * @var array $cert_rules
 * 
 * Tracks state rules for `Certified Copy` and `Certificate of Good Standing` requirements.
 * Used only on sites with a foreign registration signup form
 */
$cert_rules = array(
	'AK' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		)
	),
	'AL' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => 'copy',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		)
	),
	'AR' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'AZ' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => 'copy',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => 'copy',
			'certificate_good_standing' => 'copy'
		)
	),
	'CA' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'CO' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		)
	),
	'CT' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'DC' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'DE' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'FL' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'GA' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'HI' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'IA' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'ID' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'IL' => array(
		'LLC' =>  array(
			'certified_copy' => 'original',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => 'original',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => 'original',
			'certificate_good_standing' => ''
		)
	),
	'IN' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'KS' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'KY' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		)
	),
	'LA' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'MA' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'MD' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'ME' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'MI' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'MN' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'MO' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'MS' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'MT' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		)
	),
	'NC' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'ND' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'NE' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'NH' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'NJ' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'NM' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'NV' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => 'copy',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => 'copy',
			'certificate_good_standing' => ''
		)
	),
	'NY' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'OH' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'OK' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'OR' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'PA' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		)
	),
	'RI' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'SC' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'SD' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'TN' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'TX' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		)
	),
	'UT' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => ''
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'VA' => array(
		'LLC' =>  array(
			'certified_copy' => 'original',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => 'original',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => 'original',
			'certificate_good_standing' => 'original'
		)
	),
	'VT' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	),
	'WA' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'WI' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'WV' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'copy'
		)
	),
	'WY' => array(
		'LLC' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'Corp' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		),
		'NP' =>  array(
			'certified_copy' => '',
			'certificate_good_standing' => 'original'
		)
	)
);