<?php 
$certFees = array(
	'AK' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 15,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 5,
			'CC' => '',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 5,
			'CC' => '',
			'COG' => ''
		)
	),
	'AL' => array(
		'LLC' =>  array(
			'CCFee' => 6.50,
			'COGFee' => 28,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 28,
			'CC' => 'copy',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 28,
			'CC' => '',
			'COG' => ''
		)
	),
	'AR' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 28,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 5,
			'COGFee' => 28,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 5,
			'COGFee' => 28,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'AZ' => array(
		'LLC' =>  array(
			'CCFee' => 50,
			'COGFee' => 45,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 45,
			'COGFee' => 45,
			'CC' => 'copy',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 45,
			'COGFee' => 45,
			'CC' => 'copy',
			'COG' => 'copy'
		)
	),
	'CA' => array(
		'LLC' =>  array(
			'CCFee' => 80,
			'COGFee' => 80,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 80,
			'COGFee' => 80,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 80,
			'COGFee' => 80,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'CO' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 0,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 0,
			'CC' => '',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 0,
			'CC' => '',
			'COG' => ''
		)
	),
	'CT' => array(
		'LLC' =>  array(
			'CCFee' => 55,
			'COGFee' => 55,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 55,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 55,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'DC' => array(
		'LLC' =>  array(
			'CCFee' => 50,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 50,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 50,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'DE' => array(
		'LLC' =>  array(
			'CCFee' => 90,
			'COGFee' => 90,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 90,
			'COGFee' => 90,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 90,
			'COGFee' => 90,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'FL' => array(
		'LLC' =>  array(
			'CCFee' => 30,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 8.75,
			'COGFee' => 9,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 8.75,
			'COGFee' => 9,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'GA' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'HI' => array(
		'LLC' =>  array(
			'CCFee' => 12.50,
			'COGFee' => 7.50,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 3,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 3,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'IA' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 5,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 5,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'ID' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'IL' => array(
		'LLC' =>  array(
			'CCFee' => 35,
			'COGFee' => 47,
			'CC' => 'original',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 25,
			'COGFee' => 47,
			'CC' => 'original',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 25,
			'COGFee' => 47,
			'CC' => 'original',
			'COG' => ''
		)
	),
	'IN' => array(
		'LLC' =>  array(
			'CCFee' => 0,
			'COGFee' => 22,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 22,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 22,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'KS' => array(
		'LLC' =>  array(
			'CCFee' => 17,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 17,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 17,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'KY' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => ''
		)
	),
	'LA' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 20,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 20,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 20,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'MA' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'MD' => array(
		'LLC' =>  array(
			'CCFee' => 45,
			'COGFee' => 45,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 40,
			'COGFee' => 40,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 40,
			'COGFee' => 40,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'ME' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 30,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 30,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 30,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'MI' => array(
		'LLC' =>  array(
			'CCFee' => 16,
			'COGFee' => 12.50,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 16,
			'COGFee' => 13,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 16,
			'COGFee' => 13,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'MN' => array(
		'LLC' =>  array(
			'CCFee' => 21,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 18,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 18,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'MO' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'MS' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 27,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 27,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 27,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'MT' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 5,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 25,
			'CC' => '',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 25,
			'CC' => '',
			'COG' => ''
		)
	),
	'NC' => array(
		'LLC' =>  array(
			'CCFee' => 12,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 12,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 12,
			'COGFee' => 12,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'ND' => array(
		'LLC' =>  array(
			'CCFee' => 20,
			'COGFee' => 20,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 25,
			'COGFee' => 20,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 25,
			'COGFee' => 20,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'NE' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'NH' => array(
		'LLC' =>  array(
			'CCFee' => 5,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'NJ' => array(
		'LLC' =>  array(
			'CCFee' => 30,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 45,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 45,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'NM' => array(
		'LLC' =>  array(
			'CCFee' => 25,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'NV' => array(
		'LLC' =>  array(
			'CCFee' => 40,
			'COGFee' => 50,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 40,
			'COGFee' => 50,
			'CC' => 'copy',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 40,
			'COGFee' => 50,
			'CC' => 'copy',
			'COG' => ''
		)
	),
	'NY' => array(
		'LLC' =>  array(
			'CCFee' => 35,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 35,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 35,
			'COGFee' => 50,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'OH' => array(
		'LLC' =>  array(
			'CCFee' => 5,
			'COGFee' => 5,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 5,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 5,
			'COGFee' => 5,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'OK' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 22,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'OR' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'PA' => array(
		'LLC' =>  array(
			'CCFee' => 43,
			'COGFee' => 43,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 50,
			'COGFee' => 43,
			'CC' => '',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 50,
			'COGFee' => 43,
			'CC' => '',
			'COG' => ''
		)
	),
	'RI' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 22,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 22,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 22,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'SC' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'SD' => array(
		'LLC' =>  array(
			'CCFee' => 20,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 25,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 25,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'TN' => array(
		'LLC' =>  array(
			'CCFee' => 22.50,
			'COGFee' => 22.50,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 20,
			'COGFee' => 20,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 20,
			'COGFee' => 20,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'TX' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 15,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 20,
			'COGFee' => 15,
			'CC' => '',
			'COG' => ''
		),
		'NP' =>  array(
			'CCFee' => 20,
			'COGFee' => 15,
			'CC' => '',
			'COG' => ''
		)
	),
	'UT' => array(
		'LLC' =>  array(
			'CCFee' => 12,
			'COGFee' => 12,
			'CC' => '',
			'COG' => ''
		),
		'Corp' =>  array(
			'CCFee' => 12,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 12,
			'COGFee' => 15,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'VA' => array(
		'LLC' =>  array(
			'CCFee' => 6,
			'COGFee' => 6,
			'CC' => 'original',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 6,
			'CC' => 'original',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 6,
			'CC' => 'original',
			'COG' => 'original'
		)
	),
	'VT' => array(
		'LLC' =>  array(
			'CCFee' => 25,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 25,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 25,
			'COGFee' => 25,
			'CC' => '',
			'COG' => 'original'
		)
	),
	'WA' => array(
		'LLC' =>  array(
			'CCFee' => 70,
			'COGFee' => 70,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 70,
			'COGFee' => 70,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 70,
			'COGFee' => 70,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'WI' => array(
		'LLC' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'WV' => array(
		'LLC' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'Corp' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		),
		'NP' =>  array(
			'CCFee' => 15,
			'COGFee' => 10,
			'CC' => '',
			'COG' => 'copy'
		)
	),
	'WY' => array(
		'LLC' =>  array(
			'CCFee' => 5,
			'COGFee' => 0,
			'CC' => '',
			'COG' => 'original'
		),
		'Corp' =>  array(
			'CCFee' => 10,
			'COGFee' => 0,
			'CC' => '',
			'COG' => 'original'
		),
		'NP' =>  array(
			'CCFee' => 10,
			'COGFee' => 0,
			'CC' => '',
			'COG' => 'original'
		)
	)
);