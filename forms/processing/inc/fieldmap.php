<?php

// Declare if signup form(s) include a physical address,
// not just a mailing address - boolean (TRUE or FALSE)
$hasPhysicalAddress = FALSE;

/**
* Registered list of allowed `form_type` options (Below)
* -------------------------------------------------------------
*/
$allowed_form_types = array(
	'Registered Agent',
	'Incorporation',
	'Foreign',
	'Mail Forwarding'
);
/**
* Registered list of allowed `form_type` options (Above)
* -------------------------------------------------------------
*/


/**
* Product IDs (Below)
* -------------------------------------------------------------
*/
$products = array(
	'registered_agent' => array(
		'AK' => '8788a95f-5f59-4f8b-a960-6ce8eb31958e',
		'AL' => '0a940478-2ba8-4e85-83b3-2c15acffae42',
		'AR' => '53281046-40be-4302-a3bc-6c30741ac997',
		'AZ' => '082a5807-25de-4230-bf8f-65c041bcb2ce',
		'CA' => '47bdd4df-8079-4411-957f-dc1c1b35e7cb',
		'CO' => '68440bf8-7b59-4a71-abb2-de1dbadf55ad',
		'CT' => '418ef597-c081-4a11-8b97-4e782b6830f4',
		'DC' => '49aca576-cebc-4edb-aae9-7f4530d3f077',
		'DE' => '0bdd3a9d-0105-480e-b0af-3da6cb0c4213',
		'FL' => 'f9878936-d73d-4418-89c1-4acff769fad7',
		'GA' => '13a1c435-8daf-4e0b-8dbd-55756897633d',
		'HI' => 'fd41bee1-b14c-438b-ab23-6d7129c0ab90',
		'IA' => 'fc54c960-1597-4a78-b2a7-079dcdfb7baa',
		'ID' => '5da4fee4-2566-490f-8dc8-a283af174c62',
		'IL' => '2c32e08a-7788-45f4-a111-8f88cd5e7d2e',
		'IN' => '40e1883d-4304-48bc-8229-4ef04a273987',
		'KS' => 'dbe23fa9-9546-4e42-a0bb-c28f39b03a69',
		'KY' => '28ce8976-87b5-498a-a59e-59ffe3143521',
		'LA' => '41890fd3-be1d-4e02-9baa-dd4db75e40c1',
		'MA' => 'fb1d6473-a4f8-4ffe-854f-f865eb51c25d',
		'MD' => 'ef9681c4-8632-495f-a1dc-524a3928d2fd',
		'ME' => 'bf5a3e80-042a-456d-bece-6564ca6bd440',
		'MI' => '1c22afe6-1ab0-41cf-85a5-246d76524aeb',
		'MN' => '649913c1-7364-4406-aa81-89508fcf69ad',
		'MO' => 'b8e2f0a1-495b-441d-90c1-af183e867469',
		'MS' => 'c591a372-9771-43a8-afa9-0ea1144126ca',
		'MT' => '3aab0aab-8d30-4ff0-bbf5-f9ad12b2f295',
		'NC' => '3bfde113-505f-449c-90c9-2506cf5431f0',
		'ND' => 'ad669424-0af8-4627-ac67-e9de5f91c800',
		'NE' => 'e6241e53-69d5-4385-8838-d9ad0efc4a94',
		'NH' => '2b80f8da-c52e-4ba2-870d-8c5739d6b9e8',
		'NJ' => '4687fc33-acb1-40ee-a99c-81fc21d7445f',
		'NM' => '46551dee-8295-48bb-a9eb-9d463e0c1d78',
		'NV' => '755a0849-38dd-46c1-a873-9b607cbe9d39',
		'NY' => '9e17ccc6-a918-49a4-921b-3f80cf275081',
		'OH' => '2837ec62-043a-448d-82eb-c4c94b43a4d9',
		'OK' => '9ff26c75-e23e-489a-a34b-f422a9ffb497',
		'OR' => '73ea665d-b6c9-42f9-b6fe-0c1f96103352',
		'PA' => '7068f4ec-6819-4088-b056-bfdf291db3e5',
		'RI' => '84780a0d-3075-4120-b81b-566a984e049c',
		'SC' => '750f44a9-b4ef-4c0d-9ecf-b578cc5aff18',
		'SD' => '1ec09f86-c578-4528-971b-10e05d846d09',
		'TN' => 'f6817374-fd3f-409c-8ad1-848376176ff7',
		'TX' => '56654a11-3549-406f-87b6-78ce66d6535e',
		'UT' => 'c51de367-2088-4cd1-ae68-009b5f6ed1b7',
		'VA' => '6d1e0a9a-35c5-4a2c-91ed-0558ea627502',
		'VT' => '6d1e0a9a-35c5-4a2c-91ed-0558ea627502',
		'WA' => '432b5a88-f8b0-49d4-b28f-d36bf3e8a642',
		'WI' => '55472f6f-2d21-4a98-9f28-9877814df802',
		'WV' => '0f4bd438-9294-4d5a-a496-3ce1bd73ccde',
		'WY' => 'f4b233e8-f858-41ee-8794-c93ec6157016',
		'PR' => '6e93349c-ed73-493f-aa4d-4539635a8621'
	),
	'filing' => array(
		'domestic' => '4c157092-d161-4954-a460-28607436c2fd',
		'foreign' => '2cd5ce65-4a2d-48d8-9a64-eb5c7bf81bb2'
	),
	'optional' => array(
		'annual_compliance' => '3f2f823a-05e6-48d9-869b-1e4227b14a69',
		'apostille' => '9b64ebfa-5f82-424e-9312-99e5f253b107',
		'change_agent' => 'd8d91040-8cb8-4dc5-82ed-ed2bd9d1aab7',
		'certificate_good_standing' => '1cea2971-85e2-4e73-98ca-fa78a7c1a8e8',
		'certificate_good_standing_with_foreign' => '1cea2971-85e2-4e73-98ca-fa78a7c1a8e8',
		'certified_copy' => '4a7969ed-f66e-407a-884b-3fb53bbbc703',
		'certified_copy_with_foreign' => '4a7969ed-f66e-407a-884b-3fb53bbbc703',
		'corp_book_and_seal' => 'cd9e5e64-b4cc-4e04-80a2-9c73257b7018',
		'mail_forwarding' => array(
			'FL Open and Scan' => '',
			'WY Forward Only' => '',
			'WY Open and Scan' => '6f07f89d-b6a6-4887-a55f-6186b0f57953'
		),
		's_corp' => '2ed4bda9-545f-4acd-a542-f6bae59c22b6',
		'tax_id' => array(
			'withSSN' => '4a958b8a-2ddf-4f73-b083-50905bab6d34',
			'noSSN' => '814d7779-9c2d-40c6-8a4d-57bee8da278c'
		)
	)
);
/**
* Product IDs (Above)
* -------------------------------------------------------------
*/


/**
* Fields array for mapping form inputs (Below)
* -------------------------------------------------------------
*/
$fields = array(
	'originating_website' => 'www.' . WEBSITE_URL, // full domain with 'www'
	'error_page' => '/error/',
	'client_ip' => $_SERVER['REMOTE_ADDR'], //REQUIRED
	'form_type' => setValue('signup-type'), //REQUIRED
	'entity_type' => setValue('entityType'),
	'company_name' => setValue('companyName'),
	'company_name_alt' => '',
	'special_notes' => '',
	'phone_notification' => 'no',
	'name_reservation' => '',
	'notify_attorney' => setValue('notify-attorney'),
	'attorney_email_address' => setValue('attorney-email'),
	'email_address' => setValue('email'), //REQUIRED
	'email_address_confirm' => '',
	'password' => setValue('password'), //REQUIRED
	'password_confirm' => '',
	'home_state' => setValue('home-state'),
	'service_states' => setValue('service-j'),
	'filing_speed' => setValue('processing-pick'),
	'alternate_billing_address' => setValue('differentAddress'),
	'business_purpose' => setValue('b-purpose'),
	'terms_of_service' => 'yes',
	'token' => setValue('token'), //REQUIRED

	'company_contact' => array(
		'first_name' => setValue('fName'), //REQUIRED
		'last_name' => setValue('lName'), //REQUIRED
		'alt_email' => '',
		'phone' => preg_replace("/[^0-9]/","",setValue('phone')),
		'alt_phone' => preg_replace("/[^0-9]/","",''),
		'fax' => preg_replace("/[^0-9]/","",''),
		'mailing_address' => array(
			'address_line1' => setValue('mailAddress'),
			'address_line2' => '',
			'address_city' => setValue('mailingCity'),
			'address_region' => setValue('mailingState'),
			'address_code' => setValue('mailingZipcode'),
			'address_country' => setValue('mailingCountry')
		)
	),

	'optional_products' => array(
		'annual_compliance' => setValue('arComp'),
		'apostille' => setValue('apostille'),
		'bank_account' => '',
		'certified_copy' => setValue('c-copy'),
		'certificate_good_standing' => '',
		'corp_book_and_seal' => setValue('book-and-seal'),
		'corp_book' => '',
		'corp_seal' => '',
		'overseas_shipping' => '',
		'mail_forwarding_boolean' => setValue('mail-forwarding-boolean'),
		'mail_forwarding' => setValue('mail-forwarding-location') . ' ' . setValue('mail-forwarding-type'),
		's_corp' => setValue('s-corp'),
		'tax_id_boolean' => setValue('tax-id-boolean'),
		'tax_id_type' => setValue('tax-id-pick')
	)

);
/**
* Fields array for mapping form inputs (Above)
* -------------------------------------------------------------
*/

//Officers / Directors / Members / Managers Fix (Below)
$temp = '';
foreach ($_POST['officers'] as $key => $value) {
        if (array_key_exists('first_name', $value)) {
            //if current item is the first_name store it in temp
            $temp = $value;
            unset($_POST['officers'][$key]);
        }    
        //last name
        else {
            $_POST['officers'][$key] = array_merge($temp, $value);
        }    
}
foreach ($_POST['directors'] as $key => $value) {
    if (array_key_exists('first_name', $value)) {
        //if current item is the first_name store it in temp
        $temp = $value;
        unset($_POST['directors'][$key]);
    }    
    //last name
    else {
        $_POST['directors'][$key] = array_merge($temp, $value);
    }    
}
foreach ($_POST['members'] as $key => $value) {
    if (array_key_exists('first_name', $value)) {
        //if current item is the first_name store it in temp
        $temp = $value;
        unset($_POST['members'][$key]);
    }    
    //last name
    else {
        $_POST['members'][$key] = array_merge($temp, $value);
    }    
}
foreach ($_POST['managers'] as $key => $value) {
    if (array_key_exists('first_name', $value)) {
        //if current item is the first_name store it in temp
        $temp = $value;
        unset($_POST['managers'][$key]);
    }    
    //last name
    else {
        $_POST['managers'][$key] = array_merge($temp, $value);
    }    
}
//Officers / Directors / Members / Managers Fix (Above)


/**
* Create `jurisdictions` and special_notrs array if it doesn't exist (Below)
* -------------------------------------------------------------
*/
$fields['jurisdictions'] = array();
$fields['special_notes'] = array();
/**
* Create `jurisdictions` and special_notrs array if it doesn't exist (Above)
* -------------------------------------------------------------
*/

/**
* Check `form_type` (Below)
* -------------------------------------------------------------
*/
// Set `form_type` if empty
if (empty($fields['form_type'])) {
	$fields['form_type'] = 'Registered Agent';
}

// If the input `form_type` is not an allowed type
if ( !in_array($fields['form_type'], $allowed_form_types) ) {
	echo 'ERROR: Form Type Not Allowed';
	die;
}
/**
* Check `form_type` (Above)
* -------------------------------------------------------------
*/


if (empty($fields['service_states'])) {
	echo 'ERROR: $fields["service_states"] is empty, you need to give me at least one state';
	die;
} 
else {

	if (is_array($fields['service_states'])) {

		foreach ($fields['service_states'] as $state) {

			if (isset($fields['filing_speed']) && $fields['filing_speed'] !== '') {
				$fields['jurisdictions'][$state] = array(
					'filing_speed' => $fields['filing_speed'][$state]
				);
			} else {
				$fields['jurisdictions'][$state] = array();
			}

		}

	} else {

		$fields['jurisdictions'][ $fields['service_states'] ] = array();

		// If `filing_speed` is defined, set it
		if (isset($fields['filing_speed']) && $fields['filing_speed'] !== '') {
			$fields['jurisdictions'][ $fields['service_states'] ]['filing_speed'] = $fields['filing_speed'];
		}

	}

}
/**
* Create `jurisdictions` array if it doesn't exist (Above)
* -------------------------------------------------------------
*/


/**
* Set Billing Address Info (Below)
* -------------------------------------------------------------
*/

// Normalize field for API
if ($fields['alternate_billing_address'] == 'yes') {
	$fields['alternate_billing_address'] = TRUE;
} else {
	$fields['alternate_billing_address'] = FALSE;
}

if ($fields['alternate_billing_address']) {
	$fields['billing_information'] = array(
		'company' => setValue('companyName'),
		'address_line' => setValue('billingAddress'),
		'address_city' => setValue('billingCity'),
		'address_region' => setValue('billingState'),
		'address_code' => setValue('billingZipcode'),
		'address_country' => setValue('billingCountry')
	);
} else {
	$fields['billing_information'] = array(
		'company' => setValue('companyName'),
		'address_line' => $fields['company_contact']['mailing_address']['address_line1'],
		'address_city' => $fields['company_contact']['mailing_address']['address_city'],
		'address_region' => $fields['company_contact']['mailing_address']['address_region'],
		'address_code' => $fields['company_contact']['mailing_address']['address_code'],
		'address_country' => $fields['company_contact']['mailing_address']['address_country']
	);
}
/**
* Set Billing Address Info (Above)
* -------------------------------------------------------------
*/


/**
* Not all forms include physical address, need to set $hasPhysicalAddress
* to TRUE (in top of this file) if the form does include that address
*/
if ($hasPhysicalAddress) {
	$fields['company_contact']['physical_address'] = array(
		'address_line1' => setValue('physical-address'),
		'address_line2' => setValue('physical-address2'),
		'address_city' => setValue('physical-city'),
		'address_region' => setValue('physical-state'),
		'address_code' => setValue('physical-code'),
		'address_country' => setValue('physical-country')
	);
}


/**
* Update address region for international countries (Below)
* -------------------------------------------------------------
*/

// Update billing address region IF country != US
if ($fields['alternate_billing_address']) {
	if ($fields['billing_information']['address_country'] !== 'US') {
		$fields['billing_information']['address_region'] = setValue('billingProvidence');
	}
}

// Update mailing address region IF country != US
if ($fields['company_contact']['mailing_address']['address_country'] !== 'US') {
	$fields['company_contact']['mailing_address']['address_region'] = setValue('mailingProvidence');

	if (!$fields['alternate_billing_address']) {
		$fields['billing_information']['address_region'] = setValue('mailingProvidence');
	}
}

// Update physical address region IF country != US
if (isset($fields['company_contact']['physical_address'])) {
	if ($fields['company_contact']['physical_address']['address_country'] !== 'US') {
		$fields['company_contact']['physical_address']['address_region'] = setValue('physicalProvidence');
	}
}

/**
* Update address region for international countries (Above)
* -------------------------------------------------------------
*/

if ( $fields['optional_products']['mail_forwarding_boolean'] === 'yes' ) {
	$fields['mail_forwarding_jurisdiction'] = setValue('mail-forwarding-location');
}


if ($fields['form_type'] === 'Mail Forwarding') {
	$products['registered_agent'] = '';
}

/**
* Registered Agent Specific Fields (Below)
* -------------------------------------------------------------
*/
if ($fields['form_type'] === 'Registered Agent') {
	$fields['change_agent'] = setValue('change-agent');

	if ($fields['change_agent'] === 'yes' || $fields['change_agent'] === TRUE) {
		$fields['change_agent'] = TRUE;
		$fields['change_agent_service'] = setValue('hire-us-change-agent');

		if ( !empty($fields['change_agent_service']) && $fields['change_agent_service'] === 'yes' ) {
			$fields['optional_products']['change_agent'] = 'yes';
		}

	} else {
		$fields['change_agent'] = FALSE;
	}
}
/**
* Registered Agent Specific Fields (Above)
* -------------------------------------------------------------
*/


function set_official_addresses($officials, $selector, &$fields) {

	// Set addresses for members/managers
	foreach ($fields[$officials] as $key => $value) {

		if( isset($fields[$officials][$key]['first_name']) ) {

			if (!isset($fields[$selector][$key])) {

				// Set $selector
				$fields[$selector][$key] = '';

			}

			if ($fields[$selector][$key] === 'Mailing Address') {

				$fields[$officials][$key]['address'] = array(
					'address_line1' => $fields['company_contact']['mailing_address']['address_line1'],
					'address_line2' => $fields['company_contact']['mailing_address']['address_line2'],
					'address_city' => $fields['company_contact']['mailing_address']['address_city'],
					'address_region' => $fields['company_contact']['mailing_address']['address_region'],
					'address_code' => $fields['company_contact']['mailing_address']['address_code'],
					'address_country' => $fields['company_contact']['mailing_address']['address_country']
				);

			} else if ($fields[$selector][$key] === 'Physical Address') {

				$fields[$officials][$key]['address'] = array(
					'address_line1' => $fields['company_contact']['physical_address']['address_line1'],
					'address_line2' => $fields['company_contact']['physical_address']['address_line2'],
					'address_city' => $fields['company_contact']['physical_address']['address_city'],
					'address_region' => $fields['company_contact']['physical_address']['address_region'],
					'address_code' => $fields['company_contact']['physical_address']['address_code'],
					'address_country' => $fields['company_contact']['physical_address']['address_country']
				);

			} else if ($fields[$selector][$key] === 'Specify Address') {

				$fields[$officials][$key]['address'] = array(
					'address_line1' => $fields[$officials][$key]['mailing_address']['address_line1'],
					'address_line2' => $fields[$officials][$key]['mailing_address']['address_line2'],
					'address_city' => $fields[$officials][$key]['mailing_address']['address_city'],
					'address_region' => $fields[$officials][$key]['mailing_address']['address_region'],
					'address_code' => $fields[$officials][$key]['mailing_address']['address_code'],
					'address_country' => $fields[$officials][$key]['mailing_address']['address_country']
				);

			} else {
				// Set field/parameter to trigger use of the local registered agent office address
				$fields[$officials][$key]['address'] = 'REGISTERED_AGENT_ADDRESS';
			}
		}
	}
}


/**
* Business formation specific fields (Below)
* -------------------------------------------------------------
*/
if ($fields['form_type'] === 'Incorporation') {

	// States that require specific business purpose
	$specific_purpose_states = array(
		'AK' => 'AK', // Alaska
		'CA' => 'CA', // California
		'CT' => 'CT', // Connecticut
		'MI' => 'MI', // Michigan
		'MD' => 'MD', // Maryland
		'MS' => 'MS', // Mississippi
		'NH' => 'NH', // New Hampshire
		'NJ' => 'NJ', // New Jersey
		'PA' => 'PA', // Pennsylvania
		'SC' => 'SC', // South Carolina
 		'WA' => 'WA', // Washington
		'WV' => 'WV'  // West Virginia
	);

	// Check if the jurisdiction(s) signed up for matches the list of states
	// that require a specific business purpose
	if (is_array($fields['jurisdictions'])) {
		if (!empty(array_intersect_key($fields['jurisdictions'], $specific_purpose_states))) {
			$fields['specific_business_purpose'] = $fields['business_purpose'];
			$fields['use_specific_business_purpose'] = true;
		} else {
			$fields['generic_business_purpose'] = $fields['business_purpose'];

			// Set business purpose if it was blank
			if ($fields['generic_business_purpose'] === '') {
				$fields['generic_business_purpose'] = 'generic business purpose';
			}
		}
	} else {
		if (in_array($fields['jurisdictions'], $specific_purpose_states)) {
			$fields['specific_business_purpose'] = $fields['business_purpose'];
			$fields['use_specific_business_purpose'] = true;
		} else {
			$fields['generic_business_purpose'] = $fields['business_purpose'];

			// Set business purpose if it was blank
			if ($fields['generic_business_purpose'] === '') {
				$fields['generic_business_purpose'] = 'generic business purpose';
			}
		}
	}

	// LLC/LLP/LLLP specific fields
	if ($fields['entity_type'] === 'LLC') {
		$fields['manage_type'] = setValue('memberOrmanager');
		$fields['members'] = array_filter( setValueArray('members'), 'array_filter' );
		$fields['managers'] = array_filter( setValueArray('managers'), 'array_filter' );
		$fields['list_members'] = '';

		// Check that at least 1 member has been added
		if (empty($fields['members'])) {
			$fields['need_members'] = ''; // Triggers validation error
		}

		// If manager-managed, check that at least 1 manager has been added
		if ($fields['manage_type'] === 'Manager_Managed') {
			if (empty($fields['managers'])) {
				$fields['need_managers'] = ''; // Triggers validation error
			}
		}

		// Set all officials' addresses
		set_official_addresses('members', 'member_address_selector', $fields);
		set_official_addresses('managers', 'manager_address_selector', $fields);

	}
	// Corporation specific fields
	else if ($fields['entity_type'] === 'Corp') {
		$fields['president'][] = array(
			'first_name' => setValue('president_fname'),
			'last_name' => setValue('president_lname'),
			'address' => array(
				'address_line1' => setValue('president-address-state'),
				'address_line2' => setValue('president-none-state'),
				'address_city' => setValue('president-city-state'),
				'address_region' => setValue('president-state-state'),
				'address_code' => setValue('president-code-state'),
				'address_country' => setValue('president-country-state')
			)
		);
		$fields['secretary'][] = array(
			'first_name' => setValue('secretary_fname'),
			'last_name' => setValue('secretary_lname'),
			'address' => array(
				'address_line1' => setValue('secretary-address-state'),
				'address_line2' => setValue('secretary-none-state'),
				'address_city' => setValue('secretary-city-state'),
				'address_region' => setValue('secretary-state-state'),
				'address_code' => setValue('secretary-code-state'),
				'address_country' => setValue('secretary-country-state')
			)
		);
		$fields['treasurer'][] = array(
			'first_name' => setValue('treasurer_fname'),
			'last_name' => setValue('treasurer_lname'),
			'address' => array(
				'address_line1' => setValue('treasurer-address-state'),
				'address_line2' => setValue('treasurer-none-state'),
				'address_city' => setValue('treasurer-city-state'),
				'address_region' => setValue('treasurer-state-state'),
				'address_code' => setValue('treasurer-code-state'),
				'address_country' => setValue('treasurer-country-state')
			)
		);


		// If officers are not directors, check that at least 1 director has been added


		$fields['officers'] = array_filter( setValueArray('officers'), 'array_filter' );
		$fields['directors'] = array_filter( setValueArray('directors'), 'array_filter' );
		$fields['shares_authorized'] = setValue('sharesAuthorized');
		$fields['par_value'] = setValue('parValue');
		$fields['shares_issued'] = '';
		$fields['common_shares_authorized'] = '';

		// Set all officials' addresses
		set_official_addresses('president', 'president_address_selector', $fields);
		set_official_addresses('secretary', 'secretary_address_selector', $fields);
		set_official_addresses('treasurer', 'treasurer_address_selector', $fields);
		set_official_addresses('directors', 'director_address_selector', $fields);
		set_official_addresses('officers', 'officer_address_selector', $fields);

		// Reset directors and officers if there are none entered
		foreach ($fields['directors'] as $key => $value) {
			if ( empty($fields['directors'][$key]['first_name']) && empty($fields['directors'][$key]['last_name']) ) {
				unset($fields['directors']);
			}
		}

		foreach ($fields['officers'] as $key => $value) {
			if ( empty($fields['officers'][$key]['first_name']) && empty($fields['officers'][$key]['last_name']) ) {
				unset($fields['officers']);
			}
		}

		$executive_officers = array(
			$fields['president'][0],
			$fields['secretary'][0],
			$fields['treasurer'][0]
		);

		$fields['officers_are_directors'] = strtolower(setValue('officerAreDirectors'));

		// If officers are directors, set all main officers as directors.
		// Otherwise, check that at least 1 director has been added.
		if ($fields['officers_are_directors'] === 'yes') {
			foreach ($executive_officers as $official) {

    			$fields['directors'][] = array(
		    		'first_name' => $official['first_name'],
		    		'last_name' => $official['last_name'],
		    		'address' => $official['address']
				);

			}
		} else {
    		if (empty($fields['directors'])) {
				$fields['need_directors'] = ''; // Triggers validation error
			}
	    }
	}
	// Nonprofit specific fields
	else if ($fields['entity_type'] === 'NP Corp') {
		$fields['directors'] = array_filter( setValueArray('directors'), 'array_filter' );

		//$fields['director_address_selectors'] = setValue('director-address-selectors');

		// Check that at least 1 director has been added
		if (empty($fields['directors'])) {
			$fields['need_directors'] = ''; // Triggers validation error
		}

		// Set all officials' addresses
		set_official_addresses('directors', 'director_address_selectors', $fields);

		$fields['nonprofit']['tax_exempt_status'] = setValue('non_profit_tax_exempt');
		$fields['nonprofit']['type'] = setValue('non_profit_type');

		//hacky quick fix to make business purpose pass, comment out when issue is fixed
		$fields['special_notes'][] = 'Business Purpose: ' . $fields['business_purpose'];
	}

	if ($fields['name_reservation'] === 'yes') {
		$fields['name_reservation_num'] = '';
	}

}

/**
* Business formation specific fields (Above)
* -------------------------------------------------------------
*/


/**
* Foreign registration specific fields (Below)
* -------------------------------------------------------------
*/
if ($fields['form_type'] === 'Foreign') {

	// Set specific business purpose, generic purpose is not allowed
	$fields['specific_business_purpose'] = $fields['business_purpose'];

	// Dropdown selector to create physical address
	$fields['physical_address_selector'] = setValue('physical-address-with-state');

	// Principle / physical address for contact is required
	if ($fields['physical_address_selector'] === 'Registered Agent') {
		$fields['company_contact']['physical_address'] = array(
			'address_line1' => $fields['company_contact']['mailing_address']['address_line1'],
			'address_line2' => $fields['company_contact']['mailing_address']['address_line2'],
			'address_city' => $fields['company_contact']['mailing_address']['address_city'],
			'address_region' => $fields['company_contact']['mailing_address']['address_region'],
			'address_code' => $fields['company_contact']['mailing_address']['address_code'],
			'address_country' => $fields['company_contact']['mailing_address']['address_country']
		);
	} else if ($fields['physical_address_selector'] === 'Mailing Address') {
		$fields['company_contact']['physical_address'] = array(
			'address_line1' => $fields['company_contact']['mailing_address']['address_line1'],
			'address_line2' => $fields['company_contact']['mailing_address']['address_line2'],
			'address_city' => $fields['company_contact']['mailing_address']['address_city'],
			'address_region' => $fields['company_contact']['mailing_address']['address_region'],
			'address_code' => $fields['company_contact']['mailing_address']['address_code'],
			'address_country' => $fields['company_contact']['mailing_address']['address_country']
		);
	} else if ($fields['physical_address_selector'] === 'Specify Address') {
		$fields['company_contact']['physical_address'] = array(
			'address_line1' => setValue('physical-address-state'),
			'address_line2' => setValue('physical-address2-state'),
			'address_city' => setValue('physical-city-state'),
			'address_region' => setValue('physical-state-state'),
			'address_code' => setValue('physical-code-state'),
			'address_country' => setValue('physical-country-state')
		);
	}

	// Update physical address region IF country != US
	if (isset($fields['company_contact']['physical_address'])) {
		if (isset($fields['company_contact']['physical_address']['address_country']) && $fields['company_contact']['physical_address']['address_country'] !== 'US') {
			$fields['company_contact']['physical_address']['address_region'] = setValue('physical-providence');
		}
	}

	// LLC/LLP/LLLP specific fields
	if ($fields['entity_type'] === 'LLC') {
		$fields['manage_type'] = setValue('memberOrmanager');
		$fields['members'] = array_filter( setValueArray('members'), 'array_filter' );
		$fields['managers'] = array_filter( setValueArray('managers'), 'array_filter' );
		$fields['member_address_selectors'] = setValue('member-address-selectors');
		$fields['manager_address_selectors'] = setValue('manager-address-selectors');
		$fields['list_members'] = '';

		// Check that at least 1 member has been added
		if (empty($fields['members'])) {
			$fields['need_members'] = ''; // Triggers validation error
		}

		// If manager-managed, check that at least 1 manager has been added
		if ($fields['manage_type'] === 'Manager_Managed') {
			if (empty($fields['managers'])) {
				$fields['need_managers'] = ''; // Triggers validation error
			}
		}

		// Set all officials' addresses
		set_official_addresses('members', 'member_address_selector', $fields);
		set_official_addresses('managers', 'manager_address_selector', $fields);

	}
	// Corporation specific fields
	else if ($fields['entity_type'] === 'Corp') {
		$fields['president'][] = array(
			'first_name' => setValue('president_fname'),
			'last_name' => setValue('president_lname'),
			'address' => array(
				'address_line1' => setValue('president-address-state'),
				'address_line2' => setValue('president-none-state'),
				'address_city' => setValue('president-city-state'),
				'address_region' => setValue('president-state-state'),
				'address_code' => setValue('president-code-state'),
				'address_country' => setValue('president-country-state')
			)
		);
		$fields['secretary'][] = array(
			'first_name' => setValue('secretary_fname'),
			'last_name' => setValue('secretary_lname'),
			'address' => array(
				'address_line1' => setValue('secretary-address-state'),
				'address_line2' => setValue('secretary-none-state'),
				'address_city' => setValue('secretary-city-state'),
				'address_region' => setValue('secretary-state-state'),
				'address_code' => setValue('secretary-code-state'),
				'address_country' => setValue('secretary-country-state')
			)
		);
		$fields['treasurer'][] = array(
			'first_name' => setValue('treasurer_fname'),
			'last_name' => setValue('treasurer_lname'),
			'address' => array(
				'address_line1' => setValue('treasurer-address-state'),
				'address_line2' => setValue('treasurer-none-state'),
				'address_city' => setValue('treasurer-city-state'),
				'address_region' => setValue('treasurer-state-state'),
				'address_code' => setValue('treasurer-code-state'),
				'address_country' => setValue('treasurer-country-state')
			)
		);

		$fields['directors'] = array_filter( setValueArray('directors'), 'array_filter' );
		$fields['officers'] = array_filter( setValueArray('officers'), 'array_filter' );
		$fields['president_address_selector'] = setValue('president-address');
		$fields['secretary_address_selector'] = setValue('secretary-address');
		$fields['treasurer_address_selector'] = setValue('treasurer-address');
		$fields['officer_address_selectors'] = setValue('officers-address');
		$fields['director_address_selectors'] = setValueArray('directors-address');
		$fields['shares_authorized'] = setValue('sharesAuthorized');
		$fields['par_value'] = setValue('parValue');
		$fields['shares_issued'] = '';
		$fields['common_shares_authorized'] = '';

		// Set all officials' addresses
		set_official_addresses('president', 'president_address_selector', $fields);
		set_official_addresses('secretary', 'secretary_address_selector', $fields);
		set_official_addresses('treasurer', 'treasurer_address_selector', $fields);
		set_official_addresses('directors', 'director_address_selectors', $fields);
		set_official_addresses('officers', 'officer_address_selectors', $fields);

		// Reset directors and officers if there are none
		foreach ($fields['directors'] as $key => $value) {
			if ( empty($fields['directors'][$key]['first_name']) && empty($fields['directors'][$key]['last_name']) ) {
				unset($fields['directors'][$key]);
			}
		}

		foreach ($fields['officers'] as $key => $value) {
			if ( empty($fields['officers'][$key]['first_name']) && empty($fields['officers'][$key]['last_name']) ) {
				unset($fields['officers'][$key]);
			}
		}

		$executive_officers = array(
			$fields['president'][0],
			$fields['secretary'][0],
			$fields['treasurer'][0]
		);

		$fields['officers_are_directors'] = setValue('officerAreDirectors');

		// If officers are directors, set all main officers as directors.
		// Otherwise, check that at least 1 director has been added.
		if ($fields['officers_are_directors'] === 'yes') {
			foreach ($executive_officers as $official) {

    			$fields['directors'][] = array(
		    		'first_name' => $official['first_name'],
		    		'last_name' => $official['last_name'],
		    		'address' => $official['address']
				);

			}
		} else {
    		if (empty($fields['directors'])) {
				$fields['need_directors'] = ''; // Triggers validation error
			}
	    }
	}
	// Nonprofit specific fields
	else if ($fields['entity_type'] === 'NP Corp') {
		$fields['directors'] = array_filter( setValueArray('directors'), 'array_filter' );

		//$fields['director_address_selectors'] = setValue('director-address-selectors');

		// Check that at least 1 director has been added
		if (empty($fields['directors'])) {
			$fields['need_directors'] = ''; // Triggers validation error
		}

		// Set all officials' addresses
		set_official_addresses('directors', 'director_address_selectors', $fields);

		$fields['nonprofit']['tax_exempt_status'] = setValue('non_profit_tax_exempt');
		$fields['nonprofit']['type'] = setValue('non_profit_type');

		//hacky quick fix to make business purpose pass, comment out when issue is fixed
		$fields['special_notes'][] = 'Business Purpose: ' . $fields['business_purpose'];
	}

	// Set required cert. products to free (included) products
	$products['optional']['certified_copy'] = $products['optional']['certified_copy_with_foreign'];
	$products['optional']['certificate_good_standing'] = $products['optional']['certificate_good_standing_with_foreign'];

	// Add foreign requirements to each jurisdiction
	foreach ($fields['jurisdictions'] as $state => $value) {

		$fields['jurisdictions'][$state]['foreign_requirements'] = array(
			'certified_copy' => $cert_rules[ $state ][ $fields['entity_type'] ]['certified_copy'],
			'certificate_good_standing' => $cert_rules[ $state ][ $fields['entity_type'] ]['certificate_good_standing']
		);

	}

}

/**
* Foreign registration specific fields (Above)
* -------------------------------------------------------------
*/


/**
* Custom Error Messages (Below)
* -------------------------------------------------------------
*/
$custom_error_messages = array();
/**
* Custom Error Messages (Above)
* -------------------------------------------------------------
*/

//free_ar_comp
$fields['free_ar_comp'] = 'yes';
$fields['free_ar_comp_state'] = setValue('service-j'); 
