<?php
/**
 * Defined CONSTANTS to be used on all forms and fieldmap
 *
 * WEBSITE_URL - Domain name without subdomain prefix (no www)
 * WEBSITE_NAME - Legal entity name for website's company, for example 'Northwest Registered Agent, LLC'
 * RA_SIGNUP_FORM_PAGE - Path to the Registered Agent Sign Up Form
 * BIZ_SIGNUP_FORM_PAGE - Path to the Business Formation Sign Up Form
 * CONTACT_FORM_PAGE - Path to the Contact Form
 * THANK_YOU_PAGE - Path to the Thank you page (This is the page that is shown after the contact form is completed)
 * LOGIN_PAGE - Path to the Login form
 * FORGOT_PASSWORD_PAGE - Path to the Forgot Password form
 * TERMS_PAGE - Path to the terms and conditions page
 * CONTACT_FORM_EMAIL - The email where the contact form submissions will be stored, usually agent@WEBSITE_URL
 * PHPMAILER_HOST - The domain used to send the contact form emails via PHP Mailer, usually smtp.mailgun.org
 * PHPMAILER_USERNAME - The username used to send contact form emails via PHP Mailer, ask IT Deparment to set this up for you, for testing use postmaster@formed.com and 0daddc19c77974f5c842450f567b516a 
 * PHPMAILER_USERNAME - The password used to send contact form emails via PHP Mailer, ask IT Deparment to set this up for you, for testing use postmaster@formed.com and 0daddc19c77974f5c842450f567b516a 
 */
define('WEBSITE_URL', 'form-a-corp.com');
define('WEBSITE_PHONE', '1231231231');
define('WEBSITE_NAME', 'Form-a-Corp');
define('RA_SIGNUP_FORM_PAGE', '/form-a-business');
define('BIZ_SIGNUP_FORM_PAGE', '/form-a-business');
define('FOREIGN_SIGNUP_FORM_PAGE', '/foreign-form/');
define('CONTACT_FORM_PAGE', '/contact-us/');
define('THANK_YOU_PAGE', '/thank-you/');
define('LOGIN_PAGE', '/login-form/');
define('FORGOT_PASSWORD_PAGE', '/forgot-my-password/');
define('TERMS_PAGE', '/terms/');
define('CONTACT_FORM_EMAIL', 'warrior@llcguardian.com');
define('PHPMAILER_HOST', 'smtp.mailgun.org');
define('PHPMAILER_USERNAME', 'postmaster@formed.com');
define('PHPMAILER_PASSWORD', '0daddc19c77974f5c842450f567b516a');