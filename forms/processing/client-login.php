<?php
session_start();
opcache_reset();
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
error_reporting(0);

include_once 'bits/website-info.php';

$originatingWebsite = 'www.' . WEBSITE_URL;

$login_credentials = array(
	'username' => $_POST['username'],
	'password' => $_POST['password'],
	'url'      => $originatingWebsite
);

$url = 'https://api.agentprocessing.com/v1/session';
$curlPost = curl_init();
curl_setopt($curlPost, CURLOPT_URL, $url);
curl_setopt($curlPost, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curlPost, CURLOPT_POST, 1);
curl_setopt($curlPost, CURLOPT_POSTFIELDS, $login_credentials);
$response = curl_exec($curlPost);
$httpcode = curl_getinfo($curlPost, CURLINFO_HTTP_CODE);
curl_close($curlPost);

$result = json_decode($response, true);

if ($httpcode == 400) {
	header("Location: /client-access?validate=0");
	exit;
} else if ($httpcode == 200) {
	$account_type = $result['type'];

	if ($account_type === 'corptools') {
		header("Location: " . $result['login_url']);
		exit;
	}
} else {
	header("Location: /client-access?validate=0");
	exit;
}

?>
<html>
<head>
	<title>Registered Agent Client Login</title>
	<style type="text/css">
		.jsdisplay {
			display: inline;
		}
		.nodisplay {
			display: none;
		}
	</style>
	<!-- This is a comment, unless loaded in IE versions 8 and below: lte = "Less than or equal gte = "greater than or equal" -->
<!--[if lt IE 9]>
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<!--<![endif]-->

</head>
<body>
	<div>
		<form name="theform" method ="post" action="https://www.llcagent.com/Client/loginaction.asp" >
			<input type="hidden" width="250" name="username" value="<?= $login_credentials['username'] ?>">
			<input type="hidden" width="250" name="password" value="<?= $login_credentials['password'] ?>">
			<input type="hidden" width="250" name="confirm" value="confirmed">
			
			<div id="displaysubmit" class="jsdisplay"> 
				<?php 
				echo "<br /><br />
				<p style='color:red;'><strong>JavaScript is disabled on your browser. Enabling JavaScript will allow your online tools to work best.</p>
				<h2>Click to login to your client account:</h2>";
				echo "<input type='submit' value='Login' />";
				?>
			</div> 
		</form>
		<script type="text/javascript">
		//<![CDATA[


		$(document).ready(function() {
			/* if useJS is true, then js enabled */
			var useJS = $("#displaysubmit").hasClass('jsdisplay');

			if(useJS){ 
				// JS Enabled, display fields for payment amount and expedite options
				//alert("js enabled");
				$("#displaysubmit").hide();
				document.theform.submit();
			} 
		});

		//]]>
		</script> 
	</div>
</body>
</html>