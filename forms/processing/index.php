<?php
session_start();
error_reporting(0);
// error_reporting(E_ALL);

// Include website specific variables
include_once 'inc/website-info.php';

// Include helper functions used in the data array
include_once '../processing-bits/functions.php';

// Maps fields from form inputs to array used in Classes
include_once 'inc/fieldmap.php';

$normalized_data = array(
	'fields' => $fields,
	'products' => $products
);

// var_dump($normalized_data);
// die;

// New cURL
$body = json_encode($normalized_data);
$url = 'https://api.agentprocessing.com/v1/run'; // Live API
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'Content-Length: ' . strlen($body)
]);
$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
$result = json_decode(curl_exec($ch), TRUE);

// var_dump($result);
// die;

if ($result['success']) {
    $_SESSION['login_url'] = $result['login_url'];
	$_SESSION['form_type'] = $normalized_data['fields']['form_type'];
	$_SESSION['item_cost'] = setValue('item-cost');
	$_SESSION['state_fees'] = setValue('state-cost');
	$_SESSION['jurisdictions'] = $normalized_data['fields']['jurisdictions'];
	$_SESSION['products'] = $normalized_data['fields']['optional_products'];
	$_SESSION['change_agent'] = $normalized_data['fields']['change_agent'];
	$_SESSION['change_agent_service'] = $normalized_data['fields']['change_agent_service'];
	header("Location: /redirect/");
	exit;

} else {
	$_SESSION['errors'] = $result['errors'];
	if ($_SESSION['errors'] == '') {
		$_SESSION['errors'] = "An Unknown Error Has Occurred. Please Try Again.";
	}

	if ($fields['form_type'] == 'Incorporation') {
		header("Location: " . BIZ_SIGNUP_FORM_PAGE);
		exit;
	}
	else if ($fields['form_type'] == 'Registered Agent') {
		header("Location: " . RA_SIGNUP_FORM_PAGE);
		exit;
	}
	else if ($fields['form_type'] == 'Foreign') {
		header("Location: " . FOREIGN_SIGNUP_FORM_PAGE);
		exit;
	}
	else {
		header("Location: " . RA_SIGNUP_FORM_PAGE);
		exit;
	}
}
