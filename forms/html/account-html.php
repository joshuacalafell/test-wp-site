<div class="step">
	<div class="content">
		<h3 class="form-header">Account Setup</h3>
		<div class="label-group">
			 <label>Email</label>
		</div>
		<div class="input-group">
			<input required type="email" name="email" <?php sessionVal('email'); ?> autocapitalize="off" autocorrect="off">
		</div>
		<div>
			<div class="label-group">
				 <label>Password</label>
			</div>
			<div class="input-group">
				<input id="password" required minlength='8' pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+*!=]).*$" type="password" name="password" <?php sessionVal('password'); ?> autocapitalize="off" autocorrect="off" data-abide-validator="checkPassword">
			</div>
		</div>
		<div class="password-strength">
			<span class="password-heading">Passwords must contain at least:</span>
			<ul>
				<li class="length">8 Characters</li>
				<li class="mixed-case">1 Capital &amp; Lowercase</li>
				<li class="has-numbers">1 Number</li>
				<li class="has-special">1 Special Character</li>
			</ul>
		</div>
	</div>
</div>
