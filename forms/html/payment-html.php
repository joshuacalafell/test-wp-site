<div class="step">
	<div class="content">
		<h3 class="form-header">Payment Information</h3>
		<div class="input-group">
			<label class="checkbox different-address left">
				<input data-name="differnetAddress" id="differentAddress" type="checkbox" <?php sessionCheckbox('differentAddress','yes');?> name="differentAddress" value="yes">
				<span>Use Different Billing Address </span>
			</label>
		</div>
		<div class="billing">
			<div class="label-group">
				<label for="billingCountry">Country</label>
			</div>
			<div class="input-group">
				<select style="width:100%;" required name="billingCountry">
					<?=countryList('mailingCountry');?>
				</select>
			</div>

			<div class="label-group">
				<label for="billingAddress">Address</label>
			</div>
			<div class="input-group">
				<input required type="text" name="billingAddress" <?php sessionVal('billingAddress'); ?>>
			</div>

			<div class="half-group first">
				<div class="label-group">
					<label for="billingCity">City</label>
				</div>
				<div class="input-group">
					<input required type="text" name="billingCity" <?php sessionVal('billingCity'); ?>>
				</div>
			</div>
			<div class="half-group last">
				<div class="label-container side-by-left">
					<label for="billingState">State / Province</label>
				</div>
				<div class="input-group us-billing">
					<select style="width:100%;" required name="billingState">
						<?=stateList('mailingState');?>
					</select>
				</div>
				<div class="input-group non-us-billing">
					<input name="billingProvidence" type="text">
				</div>
			</div>

			<div class="label-container side-by-left">
				<label for="billingZipcode">Zip / Postal</label>
			</div>
			<div class="input-group us">
				<input required type="text" name="billingZipcode" <?php sessionVal('billingZipcode'); ?>>
			</div>
		</div>
		<div class="half-group first">
			<div class="label-group">
				<label>First Name on Card</label>
			</div>
			<div class="input-group">
				<input data-tokenizer-field="first_name" required type="text" <?php sessionVal('billingFName'); ?>>
			</div>
		</div>
		<div class="half-group last">
			<div class="label-group">
				<label>Last Name on Card</label>
			</div>
			<div class="input-group">
				<input data-tokenizer-field="last_name" required type="text" <?php sessionVal('billingLName'); ?>>
			</div>
		</div>

		<div class="label-group">
			<label>Card Number</label>
		</div>
		<div class="input-group">
			<input required maxlength="16" type="text" pattern="[0-9]*" data-validator="cardNumber" data-tokenizer-field="number">
		</div>
		<div class="half-group first">
			<div class="label-group">
				<label>Security Code</label>
			</div>
			<div class="input-group">
				<input required maxlength="4" type="text" data-tokenizer-field="cvc" pattern="[0-9]*" <?php sessionVal('cvvCode');?>>
			</div>
		</div>
		<div class="half-group last">
			<div class="half-group first">
				<div class="label-group">
					<label class="clear">Exp Month</label>
				</div>
				<div class="input-group parsley-fixer">
					<select class="expire" style="width:100%;" required data-tokenizer-field="exp_month">
						<option selected disabled value="">Month</option>
						<option value="1">01 - Jan</option>
						<option value="2">02 - Feb</option>
						<option value="3">03 - Mar</option>
						<option value="4">04 - Apr</option>
						<option value="5">05 - May</option>
						<option value="6">06 - June</option>
						<option value="7">07 - July</option>
						<option value="8">08 - Aug</option>
						<option value="9">09 - Sept</option>
						<option value="10">10 - Oct</option>
						<option value="11">11 - Nov</option>
						<option value="12">12 - Dec</option>
					</select>
				</div>
			</div>
			<div class="half-group last">
				<div class="label-group">
					<label class="clear">Exp Year</label>
				</div>
				<div class="input-group parsley-fixer">
					<select class="expire" style="width:100%;" required data-tokenizer-field="exp_year">
						<option selected disabled value="">Year</option>
						<?php
						$currentYear = date('Y');
						$lastYear = $currentYear + 10;
						for($i=$currentYear; $i<=$lastYear; $i++) {
							$date = new DateTime($i .'-01-01');
							echo '<option value="'. $i .'">'. $i .'</option>';
						}
						?>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>