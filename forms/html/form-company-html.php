<div class="step">
	<div class="content">
		<h3 class="form-header"><span class="e-type-fillin">[ENTITY]</span> Information</h3>
		<div class="corporation">
			<div class="label-group">
				<label>President's Name</label>
			</div>
			<div class="half-group first">
				<div class="input-group">
					<input required type="text" name="president_fname" <?php sessionVal('president_fname'); ?> placeholder="First Name">
				</div>
			</div>
			<div class="half-group last">
				<div class="input-group">
					<input required type="text" name="president_lname" <?php sessionVal('president_lname'); ?> placeholder="Last Name">
				</div>
			</div>
			<div class="label-group">
				<label>Secretary's Name</label>
			</div>
			<div class="half-group first">
				<div class="input-group">
					<input required type="text" name="secretary_fname" <?php sessionVal('secretary_fname'); ?> placeholder="First Name">
				</div>
			</div>
			<div class="half-group last">
				<div class="input-group">
					<input required type="text" name="secretary_lname" <?php sessionVal('secretary_lname'); ?> placeholder="Last Name">
				</div>
			</div>
			<div class="label-group">
				<label>Treasurer's Name</label>
			</div>
			<div class="half-group first">
				<div class="input-group">
					<input required type="text" name="treasurer_fname" <?php sessionVal('treasurer_fname'); ?> placeholder="First Name">
				</div>
			</div>
			<div class="half-group last">
				<div class="input-group">
					<input required type="text" name="treasurer_lname" <?php sessionVal('treasurer_lname'); ?> placeholder="Last Name">
				</div>
			</div>
			<div class="half-group first">
				<div class="label-group">
					<label>Number of Authorize Shares </label>
				</div>
				<div class="input-group">
					<input required type="text" name="sharesAuthorized" <?php sessionVal('sharesAuthorized'); ?>>
				</div>
			</div>
			<div class="half-group last">
				<div class="label-group">
					<label>Par Value</label>
				</div>
				<div class="input-group">
					<input required type="text" name="parValue" <?php sessionVal('parValue'); ?>>
				</div>
			</div>
			<div class="label-group">
				<label>All officers are directors</label>
			</div>
			<div class="input-group">
				<select style="width:100%;" class="no-search" required name="officerAreDirectors">
					<option <?php sessionSelect('officerAreDirectors', 'yes' )?> value="yes">Yes</option>
					<option <?php sessionSelect('officerAreDirectors', 'no' )?> value="no">No</option>
				</select>
			</div>
			<div class="officers text-center">
				<?php addPerson('officers', 'corporation', 'Officer'); ?>
				<span class="button addName">Add an Officer</span>
			</div>
			<div class="directors text-center">
				<?php addPerson('directors', 'corporation', 'Director'); ?>
				<span class="button addName">Add a Director</span>
			</div>
		</div>
		<div class="llc">
			<div class="label-group">
				<label>Managed Type</label>
			</div>
			<div class="input-group">
				<select style="width:100%;" class="no-search" required name="memberOrmanager">
					<option <?php sessionSelect('memberOrmanager', 'yes' )?> value="Manager_Managed">Manager Managed</option>
					<option <?php sessionSelect('memberOrmanager', 'no' )?> value="Member_Managed">Member Managed</option>
				</select>
			</div>
			<div class="members text-center">
				<?php addPerson('members', 'llc', 'Member'); ?>
				<span class="addName button">Add a Member</span>
			</div>
			<div class="managers text-center">
				<?php addPerson('managers', 'llc', 'Manager'); ?>
				<span class="button addName">Add a Manager</span>
			</div>
		</div>
		<div class="np">
			<div class="label-group">
				<label>Type of Non-Profit</label>
			</div>
			<div class="input-group">
				<select required name="non_profit_type">
					<option disabled selected>Select an Option</option>
					<option value="public benefit corp">Public Benefit Corporation</option>
					<option value="mutual benefit corp">Mutual Benefit Corporation</option>
					<option value="religious corp">Religious Corporation</option>
				</select>
			</div>
			<div class="label-group">
				<label>Apply For Tax Exempt Status</label>
			</div>
			<div class="input-group">
				<select required name="non_profit_tax_exempt">
					<option disabled selected>Select an Option</option>
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
			</div>
			<div class="directors text-center">
				<?php addPerson('directors', 'np', 'Director'); ?>
				<span class="button addName">Add a Director</span>
			</div>

                
                            
		</div>
	</div>
</div>