<div class="step">
	<h3 class="form-header">Optional Items</h3>
	<div class="input-group optional ar-container">
		<label class="checkbox"><input data-name="Annual Report Compliance" data-price="0" checked type="checkbox" value="yes" name="arComp">
			Annual Report Compliance &ndash; $<span class="item-cost ar-item-cost">0</span>
		</label>
		<i class="fa fa-question-circle help-text-icon" data-help-text="Every anniversary date of your company, you are required to file an annual report. Your company will get dissolved by the state if this is not done properly. The person filing the annual report gets their personal name, email, phone, address, IP and MAC address, and all their contact information listed on the permanent public records. 90 days before your annual report is due, we will send you a notice reminding you of the upcoming due date. 60 days before the annual report is due, we will automatically file your annual report and charge your card $100.00 plus state fees. You can cancel this service at any time with 1 click in your online account." aria-hidden="true"><span class="help-text-container"><strong>Bold Copy </strong>Help text body</span></i>
	</div>
	<div class="input-group optional checkbox-wrap">
		<input class="styled-checkbox" data-name="S Corporation" data-price="50" type="checkbox" value="yes" name="s-corp" id="s-corp">
		<label class="checkbox" for="s-corp">
			S-Corporation Tax Election &ndash; $<span class="item-cost">50.00</span>
		</label>
		<i class="fa fa-question-circle help-text-icon" data-help-text="" aria-hidden="true"><span class="help-text-container"><strong>Bold Copy </strong>Help text body</span></i>
	</div>
	<div class="input-group optional checkbox-wrap">
		<input class="styled-checkbox" data-name="Corporate Book And Seal" data-price="100" type="checkbox" value="yes" name="book-and-seal" id="book-and-seal">
		<label class="checkbox" for="book-and-seal">
			Corporate Book And Seal &ndash; $<span class="item-cost">100.00</span>
		</label>
	</div>
	<div class="input-group optional checkbox-wrap">
		<input class="styled-checkbox" data-name="Certified Copy" data-price="50" type="checkbox" value="yes" name="c-copy" id="c-copy">
		<label class="checkbox" for="c-copy">
			Certified Copy &ndash; $<span class="item-cost">50.00</span>
		</label>
	</div>
	<div class="input-group optional checkbox-wrap">
		<input class="styled-checkbox" data-name="Apostille" data-price="300" type="checkbox" value="yes" name="apostille" id="apostille">
		<label class="checkbox" for="apostille">
			Apostille &ndash; $<span class="item-cost">300.00</span>
		</label>
	</div>
	<div class="input-group optional checkbox-wrap">
		<input class="styled-checkbox" data-name="Wyoming Mail Forwarding" data-price="50" type="checkbox" value="yes" name="mail-forwarding-boolean" id="mail-forwarding-boolean">
		<label class="checkbox" for="mail-forwarding-boolean">
			<input type="hidden" name="mail-forwarding-type" value="Open and Scan">
			<input type="hidden" name="mail-forwarding-location" value="WY">
			Wyoming Mail Forwarding &ndash; $<span class="item-cost">50.00</span>
		</label>
		<i class="fa fa-question-circle help-text-icon" data-help-text="" aria-hidden="true"><span class="help-text-container"><strong>Bold Copy </strong>Help text body</span></i>
	</div>
	<?php 
	//hide tax id on the foreign registration form
	if (!is_page_template('page-templates/foreign-registration-form.php')):?>
	<div class="input-group checkbox-wrap">
		<input type="checkbox" value="yes" name="tax-id-boolean" id="tax-id-boolean" class="styled-checkbox">
		<label class="checkbox" for="tax-id-boolean">
			Tax ID
		</label>
	</div>
	<div class="tax-id-group">
		<div class="input-group optional checkbox-wrap">
			<input class="styled-radio" data-name="Tax ID" id="tax-id-w-ssn" data-price="50" type="radio" value="SSN" name="tax-id-pick" style="width:auto;">
			<label class="" for="tax-id-w-ssn">
				Tax ID &ndash; $<span class="item-cost">50.00</span>
			</label>
		</div>
		<div class="input-group optional checkbox-wrap">
			<input class="styled-radio" data-name="Tax ID - No SSN" id="tax-id-wo-ssn" data-price="199" type="radio" value="noSSN" name="tax-id-pick" style="width:auto;">
			<label class="" for="tax-id-wo-ssn">
				Tax ID (NO SSN) &ndash; $<span class="item-cost">199.00</span>
			</label>
		</div>
	</div>
	<?php endif;?>
</div>