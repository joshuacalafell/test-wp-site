<div class="step">
	<div class="content">
		<h3 class="form-header">Personal Details</h3>
		<div class="first half-group">
			<div class="label-group">
				<label for="fName">First Name</label>
			</div>
			<div class="input-group">
				<input class="mailAddy" required type="text" name="fName" <?php sessionVal('fName'); ?>>
			</div>
		</div>
		<div class="last half-group">
			<div class="label-group">
				<label for="lName">Last Name</label>
			</div>
			<div class="input-group">
				<input class="mailAddy" required type="text" name="lName" <?php sessionVal('lName'); ?>>
			</div>
		</div>
		<div class="half-group first">
			<div class="label-group">
				<label for="phone">Phone</label>
			</div>
			<div class="input-group">
				<input required type="tel" data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" name="phone" <?php sessionVal('phone'); ?>>
			</div>
		</div>
		<div class="half-group last">
			<div class="label-group">
				<label for="mailingCountry">Country</label>
			</div>
			<div class="input-group">
				<select style="width:100%;" required name="mailingCountry">
					<?=countryList('mailingCountry');?>
				</select>
			</div>
		</div>
		<div class="label-group">
			<label for="mailAddress">Address</label>
		</div>
		<div class="input-group">
			<input required type="text" name="mailAddress" <?php sessionVal('mailAddress'); ?>>
		</div>
		<div class="half-group first">
			<div class="label-group">
				<label for="mailingCity">City</label>
			</div>
			<div class="input-group">
				<input required type="text" name="mailingCity" <?php sessionVal('mailingCity'); ?>>
			</div>
		</div>
		<div class="half-group last">
			<div class="label-group">
				<label for="mailingState">State / Province</label>
			</div>
			<div class="input-group us-mailing">
				<select style="width:100%;" required name="mailingState">
					<?=stateList('mailingState');?>
				</select>
			</div>
			<div class="input-group non-us-mailing">
				<input name="mailingProvidence" type="text">
			</div>
		</div>
		<div class="label-container side-by-left">
			<label for="mailingZipcode">Zip / Postal</label>
		</div>
		<div class="input-group us">
			<input required type="text" name="mailingZipcode" <?php sessionVal('mailingZipcode'); ?>>
		</div>
	</div>
</div>