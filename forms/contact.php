<?php
session_start(); //start a session
error_reporting(0); //turn off error reporting

//Include required functions and other good stuff
include('processing-bits/functions.php');
include('processing-bits/form-bits.php');
include('processing/inc/website-info.php');

//Define some variables
$errors = array();
$message = setValue('message');
$fullname = setValue('name');
$email = setValue('email');
$fakeInput = setValue('fake-input'); //spam prevention
$success = FALSE;
$ip = $_SERVER['REMOTE_ADDR'];

//A little server side input validation
//If any of the required inputs are empty add a message to the errors array
if(empty($message)): $errors[] .= 'Message is required'; endif;
if(empty($fullname)): $errors[] .= 'Name is required'; endif;
if(empty($email)): $errors[] .= 'Email is required'; endif;
if(filter_var($email, FILTER_VALIDATE_EMAIL)) {} else { if(!empty($email)){$errors[] .= 'Provide a valid email.';}}

//if there are errors store them in a session called errors and send that user back to the contact page to fix that shit
if ($errors):
	$errorValues = '<ul class="errors">';
	foreach($errors as $error):
		$errorValues .= '<li>' . $error . '</li>';
	endforeach;
	$errorValues .= '</ul>';
	$_SESSION['contactErrors'] = $errorValues;
	header("Location: " . CONTACT_FORM_PAGE);
	exit;
endif;

//spam prevention
//if $fakeInput is not empty then a bot filled out the form. Send the bot to the thank you page so they think the email was sent
if ($fakeInput !== '') {
	header("Location: " . THANK_YOU_PAGE);
}

//create the mail object and load it with the required creds
include 'processing-bits/smtp_mailgun.php';
ob_start();
include('emails/contact-email.php');
$message = ob_get_clean();
$mail->Subject = 'New Contact Inquiry for ' . WEBSITE_URL;
$mail->Body =  $message;

//attempt to send the message and store if it was sucessful 
$success = $mail->send();

//if success is true sent them to the thank you page
if($success) {
	 header("Location: " . THANK_YOU_PAGE);
} 
else {
	echo 'Something went wrong. Message could not be sent.';
	echo 'Mailer Error: ' . $mail->ErrorInfo;
}

exit;