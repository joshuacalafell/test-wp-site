<?php
function sessionVal($var) {
	if(isset($_SESSION[$var])){ 
		echo 'value="' . $_SESSION[$var] . '"';
		unset($_SESSION[$var]);
	}
}

function sessionValTA($var) {
	if(isset($_SESSION[$var])){ 
		echo $_SESSION[$var];
		unset($_SESSION[$var]);
	}
}

function sessionSelect($var,$val) {
	if(isset($_SESSION[$var]) && $_SESSION[$var] === $val) { 
		echo 'selected="selected"';
		unset($_SESSION[$var]);
	} else if(!isset($_SESSION[$var]) && $val == '') {
		echo 'selected="selected"';
	}
}

function sessionSelectReturn($var,$val) {
	if(isset($_SESSION[$var]) && $_SESSION[$var] === $val) { 
		return 'selected="selected"';
		unset($_SESSION[$var]);
	} else if(!isset($_SESSION[$var]) && $val == '') {
		return 'selected="selected"';
	}
}

function sessionCheckbox($var,$val) {
	if(isset($_SESSION[$var])) { 
		if( $_SESSION[$var] === $val ) {
			echo 'checked ';
		}
		unset($_SESSION[$var]);
	}
}

function setValue($var){
	if(isset($_POST[$var])) {
		$$var = $_POST[$var];
		$_SESSION[$var] = $$var;
		return $$var;
	} else {
		$_SESSION[$var] = '';
		return ''; 
	}
}

function setValueArray($var){
	if(isset($_POST[$var])) {
		$_SESSION[$var] = $_POST[$var];
		return $_POST[$var];
	} else {
		$_SESSION[$var] = '';
		return array(); 
	}
}

function printable_error($errors) {
	if (isset($errors)) {
		// echo '<ul>';
		if (is_array($errors)) {
			foreach ($errors as $e) {

				if (is_array($e)) {
					printable_error($e);
				} else {
					echo '<li>'. $e .'</li>';
				}
			}
		} else {
			echo '<li>'. $errors .'</li>';
		}
		// echo '</ul>';
	}
}