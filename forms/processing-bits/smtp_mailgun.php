<?php
	//opcache_reset();
	require 'PHPMailerAutoload.php';
	$mail = new PHPMailer; //create the mail obj

	//COMMENT OUT if env vars are enabled
	$mail->isSMTP();
	$mail->Host = PHPMAILER_HOST;
	$mail->SMTPAuth = true;
	$mail->Username = PHPMAILER_USERNAME;
	$mail->Password = PHPMAILER_PASSWORD;
	$mail->SMTPSecure = 'tls';
	$mail->Port = '25';

	//UNCOMMENT if env vars are enabled 
	// $mail->isSMTP();                                      // Set mailer to use SMTP
	// $mail->Host = $_ENV['MG_SMTP_HOST'];                     // Specify main and backup SMTP servers
	// $mail->SMTPAuth = true;                               // Enable SMTP authentication
	// $mail->Username = $_ENV['MG_SMTP_USERNAME'];   // SMTP username
	// $mail->Password = $_ENV['MG_SMTP_PASSWORD'];  // SMTP password
	// $mail->SMTPSecure = 'tls';
	// $mail->Port = '25';

	$mail->IsHTML(true);
	$mail->From = CONTACT_FORM_EMAIL;
	$mail->FromName = WEBSITE_NAME;
	$mail->addAddress(CONTACT_FORM_EMAIL); 
	$mail->addBCC('jt@corporatetools.com'); //BBC ME HERE
	$mail->addReplyTo($email, $fullname);
	$mail->WordWrap = 50;     // Set word wrap to 50 characters