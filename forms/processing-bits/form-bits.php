<?php

$statesTerritoriesArray = array(
	'AK' => 'Alaska',
	'AL' => 'Alabama',
	'AR' => 'Arkansas',
	'AZ' => 'Arizona',
	'CA' => 'California',
	'CO' => 'Colorado',
	'CT' => 'Connecticut',
	'DC' => 'District of Columbia',
	'DE' => 'Delaware',
	'FL' => 'Florida',
	'GA' => 'Georgia',
	'HI' => 'Hawaii',
	'IA' => 'Iowa',
	'ID' => 'Idaho',
	'IL' => 'Illinois',
	'IN' => 'Indiana',
	'KS' => 'Kansas',
	'KY' => 'Kentucky',
	'LA' => 'Louisiana',
	'MA' => 'Massachusetts',
	'MD' => 'Maryland',
	'ME' => 'Maine',
	'MI' => 'Michigan',
	'MN' => 'Minnesota',
	'MO' => 'Missouri',
	'MS' => 'Mississippi',
	'MT' => 'Montana',
	'NC' => 'North Carolina',
	'ND' => 'North Dakota',
	'NE' => 'Nebraska',
	'NH' => 'New Hampshire',
	'NJ' => 'New Jersey',
	'NM' => 'New Mexico',
	'NV' => 'Nevada',
	'NY' => 'New York',
	'OH' => 'Ohio',
	'OK' => 'Oklahoma',
	'OR' => 'Oregon',
	'PA' => 'Pennsylvania',
	'RI' => 'Rhode Island',
	'SC' => 'South Carolina',
	'SD' => 'South Dakota',
	'TN' => 'Tennessee',
	'TX' => 'Texas',
	'UT' => 'Utah',
	'VA' => 'Virginia',
	'VT' => 'Vermont',
	'WA' => 'Washington',
	'WI' => 'Wisconsin',
	'WV' => 'West Virginia',
	'WY' => 'Wyoming',

	'US Military' => array(
		'AA' => 'Armed Forces Americas',
		'AE' => 'Armed Forces Europe',
		'AP' => 'Armed Forces Pacific'
		),

	'US Territories' => array(
		'AS' => 'American Samoa',
		'GU' => 'Guam',
		'MP' => 'Northern Mariana Islands',
		'PR' => 'Puerto Rico',
		'VI' => 'US Virgin Islands'
		)
	);

$countryArray = array(
	'AF' => 'Afghanistan',
	'AX' => 'Åland',
	'AL' => 'Albania',
	'DZ' => 'Algeria',
	'AS' => 'American Samoa',
	'AD' => 'Andorra',
	'AO' => 'Angola',
	'AI' => 'Anguilla',
	'AQ' => 'Antarctica',
	'AG' => 'Antigua and Barbuda',
	'AR' => 'Argentina',
	'AM' => 'Armenia',
	'AW' => 'Aruba',
	'AU' => 'Australia',
	'AT' => 'Austria',
	'AZ' => 'Azerbaijan',
	'BS' => 'Bahamas',
	'BH' => 'Bahrain',
	'BD' => 'Bangladesh',
	'BB' => 'Barbados',
	'BY' => 'Belarus',
	'BE' => 'Belgium',
	'BZ' => 'Belize',
	'BJ' => 'Benin',
	'BM' => 'Bermuda',
	'BT' => 'Bhutan',
	'BO' => 'Bolivia',
	'BQ' => 'Bonaire',
	'BA' => 'Bosnia and Herzegovina',
	'BW' => 'Botswana',
	'BV' => 'Bouvet Island',
	'BR' => 'Brazil',
	'IO' => 'British Indian Ocean Territory',
	'VG' => 'British Virgin Islands',
	'BN' => 'Brunei',
	'BG' => 'Bulgaria',
	'BF' => 'Burkina Faso',
	'BI' => 'Burundi',
	'KH' => 'Cambodia',
	'CM' => 'Cameroon',
	'CA' => 'Canada',
	'CV' => 'Cape Verde',
	'KY' => 'Cayman Islands',
	'CF' => 'Central African Republic',
	'TD' => 'Chad',
	'CL' => 'Chile',
	'CN' => 'China',
	'CX' => 'Christmas Island',
	'CC' => 'Cocos [Keeling] Islands',
	'CO' => 'Colombia',
	'KM' => 'Comoros',
	'CK' => 'Cook Islands',
	'CR' => 'Costa Rica',
	'HR' => 'Croatia',
	'CU' => 'Cuba',
	'CW' => 'Curacao',
	'CY' => 'Cyprus',
	'CZ' => 'Czech Republic',
	'CD' => 'Democratic Republic of the Congo',
	'DK' => 'Denmark',
	'DJ' => 'Djibouti',
	'DM' => 'Dominica',
	'DO' => 'Dominican Republic',
	'TL' => 'East Timor',
	'EC' => 'Ecuador',
	'EG' => 'Egypt',
	'SV' => 'El Salvador',
	'GQ' => 'Equatorial Guinea',
	'ER' => 'Eritrea',
	'EE' => 'Estonia',
	'ET' => 'Ethiopia',
	'FK' => 'Falkland Islands',
	'FO' => 'Faroe Islands',
	'FJ' => 'Fiji',
	'FI' => 'Finland',
	'FR' => 'France',
	'GF' => 'French Guiana',
	'PF' => 'French Polynesia',
	'TF' => 'French Southern Territories',
	'GA' => 'Gabon',
	'GM' => 'Gambia',
	'GE' => 'Georgia',
	'DE' => 'Germany',
	'GH' => 'Ghana',
	'GI' => 'Gibraltar',
	'GR' => 'Greece',
	'GL' => 'Greenland',
	'GD' => 'Grenada',
	'GP' => 'Guadeloupe',
	'GU' => 'Guam',
	'GT' => 'Guatemala',
	'GG' => 'Guernsey',
	'GN' => 'Guinea',
	'GW' => 'Guinea-Bissau',
	'GY' => 'Guyana',
	'HT' => 'Haiti',
	'HM' => 'Heard Island and McDonald Islands',
	'HN' => 'Honduras',
	'HK' => 'Hong Kong',
	'HU' => 'Hungary',
	'IS' => 'Iceland',
	'IN' => 'India',
	'ID' => 'Indonesia',
	'IR' => 'Iran',
	'IQ' => 'Iraq',
	'IE' => 'Ireland',
	'IM' => 'Isle of Man',
	'IL' => 'Israel',
	'IT' => 'Italy',
	'CI' => 'Ivory Coast',
	'JM' => 'Jamaica',
	'JP' => 'Japan',
	'JE' => 'Jersey',
	'JO' => 'Jordan',
	'KZ' => 'Kazakhstan',
	'KE' => 'Kenya',
	'KI' => 'Kiribati',
	'XK' => 'Kosovo',
	'KW' => 'Kuwait',
	'KG' => 'Kyrgyzstan',
	'LA' => 'Laos',
	'LV' => 'Latvia',
	'LB' => 'Lebanon',
	'LS' => 'Lesotho',
	'LR' => 'Liberia',
	'LY' => 'Libya',
	'LI' => 'Liechtenstein',
	'LT' => 'Lithuania',
	'LU' => 'Luxembourg',
	'MO' => 'Macao',
	'MK' => 'Macedonia',
	'MG' => 'Madagascar',
	'MW' => 'Malawi',
	'MY' => 'Malaysia',
	'MV' => 'Maldives',
	'ML' => 'Mali',
	'MT' => 'Malta',
	'MH' => 'Marshall Islands',
	'MQ' => 'Martinique',
	'MR' => 'Mauritania',
	'MU' => 'Mauritius',
	'YT' => 'Mayotte',
	'MX' => 'Mexico',
	'FM' => 'Micronesia',
	'MD' => 'Moldova',
	'MC' => 'Monaco',
	'MN' => 'Mongolia',
	'ME' => 'Montenegro',
	'MS' => 'Montserrat',
	'MA' => 'Morocco',
	'MZ' => 'Mozambique',
	'MM' => 'Myanmar [Burma]',
	'NA' => 'Namibia',
	'NR' => 'Nauru',
	'NP' => 'Nepal',
	'NL' => 'Netherlands',
	'NC' => 'New Caledonia',
	'NZ' => 'New Zealand',
	'NI' => 'Nicaragua',
	'NE' => 'Niger',
	'NG' => 'Nigeria',
	'NU' => 'Niue',
	'NF' => 'Norfolk Island',
	'KP' => 'North Korea',
	'MP' => 'Northern Mariana Islands',
	'NO' => 'Norway',
	'OM' => 'Oman',
	'PK' => 'Pakistan',
	'PW' => 'Palau',
	'PS' => 'Palestine',
	'PA' => 'Panama',
	'PG' => 'Papua New Guinea',
	'PY' => 'Paraguay',
	'PE' => 'Peru',
	'PH' => 'Philippines',
	'PN' => 'Pitcairn Islands',
	'PL' => 'Poland',
	'PT' => 'Portugal',
	'PR' => 'Puerto Rico',
	'QA' => 'Qatar',
	'CG' => 'Republic of the Congo',
	'RE' => 'Réunion',
	'RO' => 'Romania',
	'RU' => 'Russia',
	'RW' => 'Rwanda',
	'BL' => 'Saint Barthélemy',
	'SH' => 'Saint Helena',
	'KN' => 'Saint Kitts and Nevis',
	'LC' => 'Saint Lucia',
	'MF' => 'Saint Martin',
	'PM' => 'Saint Pierre and Miquelon',
	'VC' => 'Saint Vincent and the Grenadines',
	'WS' => 'Samoa',
	'SM' => 'San Marino',
	'ST' => 'São Tomé and Príncipe',
	'SA' => 'Saudi Arabia',
	'SN' => 'Senegal',
	'RS' => 'Serbia',
	'SC' => 'Seychelles',
	'SL' => 'Sierra Leone',
	'SG' => 'Singapore',
	'SX' => 'Sint Maarten',
	'SK' => 'Slovakia',
	'SI' => 'Slovenia',
	'SB' => 'Solomon Islands',
	'SO' => 'Somalia',
	'ZA' => 'South Africa',
	'GS' => 'South Georgia and the South Sandwich Islands',
	'KR' => 'South Korea',
	'SS' => 'South Sudan',
	'ES' => 'Spain',
	'LK' => 'Sri Lanka',
	'SD' => 'Sudan',
	'SR' => 'Suriname',
	'SJ' => 'Svalbard and Jan Mayen',
	'SZ' => 'Swaziland',
	'SE' => 'Sweden',
	'CH' => 'Switzerland',
	'SY' => 'Syria',
	'TW' => 'Taiwan',
	'TJ' => 'Tajikistan',
	'TZ' => 'Tanzania',
	'TH' => 'Thailand',
	'TG' => 'Togo',
	'TK' => 'Tokelau',
	'TO' => 'Tonga',
	'TT' => 'Trinidad and Tobago',
	'TN' => 'Tunisia',
	'TR' => 'Turkey',
	'TM' => 'Turkmenistan',
	'TC' => 'Turks and Caicos Islands',
	'TV' => 'Tuvalu',
	'UM' => 'U.S. Minor Outlying Islands',
	'VI' => 'U.S. Virgin Islands',
	'UG' => 'Uganda',
	'UA' => 'Ukraine',
	'AE' => 'United Arab Emirates',
	'GB' => 'United Kingdom',
	'US' => 'United States',
	'UY' => 'Uruguay',
	'UZ' => 'Uzbekistan',
	'VU' => 'Vanuatu',
	'VA' => 'Vatican City',
	'VE' => 'Venezuela',
	'VN' => 'Vietnam',
	'WF' => 'Wallis and Futuna',
	'EH' => 'Western Sahara',
	'YE' => 'Yemen',
	'ZM' => 'Zambia',
	'ZW' => 'Zimbabwe'
	);

function countryList($fieldName){
	global $countryArray;
	$countryOptions = '';
	foreach ($countryArray as $countryCode => $countryName) {
		$countryOptions .= '<option ' . sessionSelectReturn($fieldName, $countryCode); if($countryName == 'United States'): $countryOptions .= 'selected'; endif; $countryOptions .= ' value="' . $countryCode . '">' . $countryName . '</option>';
	}
	unset($_SESSION[$fieldName]);
	return $countryOptions;
}

function stateList($fieldName){
	global $statesTerritoriesArray;
	$stateList = '<option selected disabled value="">Select State</option>';
	foreach ($statesTerritoriesArray as $key => $value) {

		if (is_array($value)) {
			$stateList .= '<optgroup label="'. $key .'">';

			foreach ($value as $k2 => $v2) {
				$stateList .= '<option ' . sessionSelectReturn($fieldName, $k2) . ' value="'. $k2 .'">'. $v2 .'</option>';
			}

			$stateList .= '</optgroup>';

		} else {
			$stateList .= '<option ' . sessionSelectReturn($fieldName, $key) . ' value="'. $key .'">'. $value .'</option>';
		}
	}
	unset($_SESSION[$fieldName]);
	return $stateList;
}

function stateOnlyList($fieldName){
	global $statesTerritoriesArray;
	$stateList = '<option selected disabled value="">Select State</option>';
	foreach ($statesTerritoriesArray as $key => $value) {
		if (!is_array($value)) {
			$stateList .= '<option ' . sessionSelectReturn($fieldName, $key) . ' value="'. $key .'">'. $value .'</option>';
		}
	}
	unset($_SESSION[$fieldName]);
	return $stateList;
}


function addPerson($person, $validator, $displayName, $form=''){
    $i = 0;
    $personTitle = rtrim(ucfirst($displayName), 's');

    echo'
    <div class="' . $person . '-' . $validator . ' person-container form-group half">';
        if (isset($_SESSION[$person]) && !empty($_SESSION[$person]) ) {
            $thePeople = $_SESSION[$person];
        } else {
            $thePeople = array(
                'person' => array(
                    'first_name' =>  '',
                    'last_name' =>  '',
                    'address' => array(
                        'address_country' => '',
                        'address_line1' => '',
                        'address_city' => '',
                        'address_region' => '',
                        'address_code' => ''
                    )
                )
            );
        }
        echo
        '<div class="person clearfix">
            <div class="label-group">
                <label>Add a '. $personTitle .'</label><i class="fa fa-times remove-person" aria-hidden="true"></i>
            </div>
            <div class="half-group first side-by-side-input left">
                <div class="input-group">
                        <input class="firstName" data-validator="' . $validator . '" type="text" name="' . $person . '[][first_name]" placeholder="First Name">
                </div>
            </div>
            <div class="half-group last side-by-side-input right">
                <div class="input-group">
                        <input class="lastName" data-validator="' . $validator . '" type="text" name="' . $person . '[][last_name]" placeholder="Last Name">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>';
    echo '</div>';
}