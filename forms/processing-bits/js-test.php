<?php
include_once 'inc/signups.inc.php'; // Include signup logging

$signup_model = new SignupLog;

// Update `javascript enabled` record
$signupID = $_POST['signupId'];

$data = array(
	'signup_id' => $signupID,
	'step_type' => 'Javascript enabled',
	'result'	=> 'Yes'
);

$update_step = $signup_model->update_step_result_by_signup_id( $data );