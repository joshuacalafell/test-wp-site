<?php get_header(); ?>
<section class="primary-content search-page">
	<div class="row the-header">
		<div class="medium-9 large-8 columns">
			<?php if(isset($_GET['s']) && !empty($_GET['s']) ): ?>
			<h1><?php echo sprintf( __( '%s Search Results for &ldquo;', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?>&rdquo;</h1>
			<?php else: ?>
				<h1>Search <?=get_theme_mod('theme_options_name', ''); ?></h1>
			<?php  endif; ?>
			<?php get_template_part('searchform'); ?>
		</div>
	</div>
	<div class="row the-content">
		<div class="medium-9 large-8 columns">
			<?php if(isset($_GET['s']) && !empty($_GET['s']) ): ?>
			<div class="section-group">
				<?php get_template_part('loop'); ?>
			</div>
			<?php endif; ?>
		</div>
		<div class="medium-3 large-4 sticky-side-bar columns"></div>
	</div>
	<div class="row">
		<div class="medium-9 large-8 columns">
			<?php get_template_part('pagination'); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>