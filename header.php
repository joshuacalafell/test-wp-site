<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="<?php echo get_template_directory_uri();?>/fontawesome/fontawesome.min.css" rel="stylesheet">
		<link href="<?php echo get_template_directory_uri();?>/fontawesome/solid.min.css" rel="stylesheet">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<!-- wrapper -->
		<div class="wrapper">
			<header class="header clear" role="banner">
				<div class="logo">
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri();?>/assets/img/placeholder_logo.png" alt="Logo" class="logo-img">
					</a>
				</div>
				<nav class="nav" role="navigation">					
					<?php html5blank_nav(); ?>
				</nav>
				<div class="mobile-nav-toggle">
					<div class="nav-icon"></div>
				</div>
			</header>
