$(function () {
  //initial setup
  initialSetup();


  //make an ajax call to inc-pricing.php which contains the state prices JSON
  var statepricing = 0;
  var arpricing = 0;
  var foreignpricing = 0;
  var homestatepricing = 0;
  var changeagentpricing = 0;

  if ($('[name="signup-type"]').val() === 'Registered Agent') {
    $.ajax({
      'url': "/wp-content/themes/theme-name-here/forms/pricing/change-agent-pricing.php",
      'dataType': "json",
      success: function (data) {
        changeagentpricing = data;
        changeRaPrice();
      }
    });
  }

  if ($('[name="signup-type"]').val() === 'Incorporation') {
    $.ajax({
      'url': "/wp-content/themes/theme-name-here/forms/pricing/inc-pricing.php",
      'dataType': "json",
      success: function (data) {
        statepricing = data;
        statePrice();
      }
    });
  }

  if ($('[name="signup-type"]').val() === 'Foreign') {
    $.ajax({
      'url': "/wp-content/themes/theme-name-here/forms/pricing/foreign-pricing.php",
      'dataType': "json",
      success: function (data) {
        foreignpricing = data;
        foreignStatePrice();
      }
    });
    $.ajax({
      'url': "/wp-content/themes/theme-name-here/forms/pricing/cert-fees.php",
      'dataType': "json",
      success: function (data) {
        homestatepricing = data;
        foreignHomeStatePrice();
      }
    });
  }

  $.ajax({
    'url': "/wp-content/themes/theme-name-here/forms/pricing/annual-report-fees.php",
    'dataType': "json",
    success: function (data) {
      arpricing = data;
      arComp();
    }
  });

  //generate processing choices
  function statePrice() {
    if ($('[name="signup-type"]').val() === 'Incorporation') {
      $('.processing-opts .input-group').empty();
      $('.processing-opts').hide();
      try {
        var myState = $('[name="service-j"]').val();
        if (myState != undefined) {
          var myEntity = $('[name="entityType"]').val();
          // console.log(myEntity);
          $('.processing-opts').show();
          for (var i = 0; i < statepricing[myState][myEntity].length; i++) {
            $('.processing-opts .input-group').append('<label class="optional state-fee-optional-p"><input required data-name="State Filing Fee" data-price="' + statepricing[myState][myEntity][i]['TotalProcessPrice'] + '" value="' + statepricing[myState][myEntity][i]['ExpeditedCode'] + '" name="processing-pick" type="radio"> <strong>$' + statepricing[myState][myEntity][i]['TotalProcessPrice'] + '</strong> - ' + statepricing[myState][myEntity][i]['FileDate'] + ' Day(s) </label>');
          }
          updateSlickHeight($('.form-step.slick-current'));
        }
      } catch (e) {
        //console.log('error caught');
      }

    }
  }

  //generate processing choices
  function foreignStatePrice() {
    if ($('[name="signup-type"]').val() === 'Foreign') {
      $('[name="home-state"] option').attr('disabled', false);
      $('.processing-opts .input-group').empty();
      $('.processing-opts').hide();
      try {
        var myState = $('[name="service-j"]').val();
        if (myState != undefined) {
          var myEntity = $('[name="entityType"]').val();
          $('.processing-opts').show();
          $('[name="home-state"] option[value="' + myState + '"]').attr('disabled', 'disabled');
          for (var i = 0; i < foreignpricing[myState][myEntity].length; i++) {
            $('.processing-opts .input-group').append('<label class="optional state-fee-optional-p"><input required data-name="State Filing Fee" data-price="' + foreignpricing[myState][myEntity][i]['TotalProcessPrice'] + '" value="' + foreignpricing[myState][myEntity][i]['ExpeditedCode'] + '" name="processing-pick" type="radio"> <strong>$' + foreignpricing[myState][myEntity][i]['TotalProcessPrice'] + '</strong> - ' + foreignpricing[myState][myEntity][i]['FileDate'] + ' Day(s) </label>');
          }
        }
      } catch (e) {
        console.log('error caught, error message: ' + e.message);
      }

    }
  }

  function foreignHomeStatePrice() {
    if ($('[name="signup-type"]').val() === 'Foreign') {
      var homeStateCOGFee = 0;
      $('[name="cogFee"]').val(homeStateCOGFee);
      $('.cert-fee .right').text('$' + homeStateCOGFee.toFixed(2));
      try {
        var homeState = $('[name="home-state"]').val();
        var serviceState = $('[name="service-j"]').val();
        var myEntity = $('[name="entityType"]').val();

        var cogRequired = true;
        if (homestatepricing[serviceState][myEntity]['COG'] == '') {
          cogRequired = false;
        }

        if (cogRequired == true) {
          homeStateCOGFee = homestatepricing[homeState][myEntity]['COGFee'];
          $('[name="cogFee"]').val(homeStateCOGFee);
          $('.cert-fee .right').text('$' + homeStateCOGFee.toFixed(2));
        }

      } catch (e) {
        console.log('error caught, error message: ' + e.message);
      }

    }
  }

  function arComp() {
    var myEntity = $('[name="entityType"]').val();
    var myState = $('[name="service-j"]').val();
    var arPrice = 0;
    var hasFiling = false;

    try {
      hasFiling = arpricing[myState][myEntity][0]['domestic'][0]['hasFiling'];
      if (hasFiling) {
        if (myEntity == 'LLC' || myEntity == 'Corp' || myEntity == 'NP Corp') {
          $('.ar-container').show();
        } else {
          $('.ar-container').hide();
        }
      }
    } catch (e) {
      //console.log(e); //error will be thrown if state or entity are not set
    }
  }

  function changeRaPrice() {
    if ($('[name="signup-type"]').val() === 'Registered Agent') {
      var myEntity = $('[name="entityType"]').val();
      var myState = $('[name="service-j"]').val();
      var total = 0;
      var baseCost = 25; //this is our service fee

      try {
        if (myEntity == 'LLC' || myEntity == 'Corp' || myEntity == 'NP Corp') {
          total = changeagentpricing[myState][myEntity][0]['price'] + baseCost;
          $('[name="hire-us-change-agent"]').attr('data-price', total);
          $('.hire-us-to-change-container .item-cost').text('$' + total.toFixed(2));
        } else {
          $('[name="hire-us-change-agent"]').attr('data-price', baseCost);
          $('.hire-us-to-change-container .item-cost').text('$' + baseCost.toFixed(2));
        }
      } catch (e) {
        // console.log(e); //error will be thrown if state or entity are not set
      }
    }
  }

  $('body').on('click', '[name="processing-pick"]', function () {
    optional();
    totalPrice();
  });

  $('[name="service-j"]').on('change', function () {
    statePrice();
    arComp();
    foreignStatePrice();
    foreignHomeStatePrice();
    optional();
    totalPrice();
  });

  $('[name="home-state"]').on('change', function () {
    foreignHomeStatePrice();
    totalPrice();
  });

  $('[name="notify-attorney"]').on('change', function () {
    showNotifyAttorney();
    updateSlickHeight($('.form-step.slick-current'));
  });

  $('#differentAddress').on('change', function () {
    showBillingAddress();
    addTokenizerData();
    swapForeignBillingCountry();
  });

  $('[name="entityType"]').on('change', function () {
    swapEntityType();
    officersDirectors();
    statePrice();
    arComp();
    changeRaPrice();
    foreignStatePrice();
    foreignHomeStatePrice();
    changeRaPrice();
    optional();
    totalPrice();
  });

  $('[name="officerAreDirectors"]').on('change', function () {
    officersDirectors();
  });

  $('[name="memberOrmanager"]').on('change', function () {
    hideManager();
    updateSlickHeight($('.form-step.slick-current'));
  });

  $('[name="mailingCountry"]').on('change', function () {
    swapForeignMailingCountry();
    addTokenizerData();
  });

  $('[name="billingCountry"]').on('change', function () {
    swapForeignBillingCountry();
    addTokenizerData();
  });

  $('[name="entityType"]').on('change', function () {
    totalPrice();
  });

  $('input[type="checkbox"]').on('change', function () {
    optional();
    totalPrice();
  });

  $('input[type="radio"]').on('change', function () {
    optional();
    totalPrice();
  });

  $('[name="password"]').on('focusin', function () {
    showPW();
    updateSlickHeight($('.form-step.slick-current'));
  });

  $('[name="password"]').on('keydown, keyup', function () {
    var passwordStatus = passwordStrength();
    if (passwordStatus == true) {
      $('.password-strength').hide();
    } else {
      $('.password-strength').show();
    }
  });

  $('[name="tax-id-boolean"]').on('click', function () {
    taxIdOptions();
  });

  $('[name="change-agent"]').on('change', function () {
    hireUsChangeAgent();
    optional();
    totalPrice();
  });


  //help text logic
  $('.help-trigger i').on('click', function () {
    var $i = $(this); //the icon
    var $helpTrigger = $(this).parent(); //the help-trigger
    var $helpContainer = $(this).next(); //the help-container
    var theHelpText = $helpTrigger.attr('data-help-text'); //grab the help text from the data attr data-help-text
    var isOpen = false; //is the help container open?

    if ($helpTrigger.hasClass('show-after')) {
      isOpen = true;
    }

    $('.help-container').hide(); //hide all help container
    $('.help-trigger').removeClass('show-after'); //hide all triangles

    if (!isOpen) { //if the helpContainer is not open, 
      $helpContainer.find('.the-text').text(theHelpText); //put the help text into the div with the class .the-text
      $helpTrigger.addClass('show-after'); //show the triangle
      $helpContainer.show(); //show the help container
    }
  });

  $('.exit-help-container').on('click', function () {
    var $helpTrigger = $(this).parent().parent();
    var $helpContainer = $(this).parent();

    $helpTrigger.removeClass('show-after'); //hide the triangle
    $helpContainer.hide(); //hide the help container
  });


  //handle the add person
  var personNumber = 2;
  $('.addName').on('click', function (e) {
    var $container = $(this).parent().parent().parent();
    //var $carWrap = $('.slick-list');
    var $clickedWrap = $(this).prev();

    var x = $(this).prev('.person-container').find('.person').length;
    if (x >= 9) {} else if (x <= 1 && !$(this).prev().find('.person').first().is(':visible')) {
      $clickedWrap.find('.person').show();

      updateSlickHeight($container);

      $clickedWrap.find('.person').hide();
      $clickedWrap.find('.person').slideDown(350);
    } else {
      var $clone = $clickedWrap.find('.person').first().clone();
      $clone.find('input').val('');
      $clone.find('div').removeAttr('style');
      $clone.find('.fa').removeAttr('style');
      $clone.insertAfter($clickedWrap.find('.person').last());
      $clickedWrap.find('.person').last().find('input').each(function () {
        var nameValue = $(this).attr('name').replace(/[(\d+)]/, personNumber);
        $(this).attr('name', nameValue);
      });

      updateSlickHeight($container);

      $clickedWrap.find('.person').last().hide();
      $clickedWrap.find('.person').last().slideDown(350);
    }
    if (x >= 8) {
      $(this).hide();
    }
    personNumber = personNumber + 1;
  });

  //handle remove person
  $('form').on('click', '.remove-person', function () {
    var x = $(this).parents('.person-container').find('.person').length;
    $(this).parents('.person-container').parent().find('.add-person').show();

    if (x <= 1) {
      $(this).parents('.person').slideUp(350);
    } else {
      $(this).parents('.person').slideUp(350);
      $(this).parents('.person').addClass('remove-me');

      setTimeout(function () {
        $('.person.remove-me').remove();
        updateSlickHeight($('.form-step.slick-current'));
      }, 350);
    }
  });

  //enter madness
  //handle form submit
  var tokenAttempts = 0;
  $('#signup [type="submit"]').on('click', function (event) {
    $(this).submit();
    event.preventDefault();
    if ($('.parsley-error').length > 0) {
      $('.parsley-error').first().focus();
      updateSlickHeight($('.form-step.slick-current'));
    } else {
      var form = $('#signup');
      $('.card-errors').hide();
      $('.processing').removeClass('hide');
      $('#signup [type="submit"]').attr('disabled', true);
      // var stateFee = $('.state-fee-optional-p input').attr('data-price');
      // $('[name="state-cost"]').val(stateFee);
      try {
        Tokenizer.createCard(form, function (card) {
          var token = card.getToken();
          form.append($('<input type="hidden" name="token" />').val(token));
          if ($('[name="token"]').length > 0 || tokenAttempts > 4) {
            form.get(0).submit();
          } else {
            setTimeout(function () {
              form.find('[type="submit"]').trigger('click');
              tokenAttempts = tokenAttempts + 1;
            }, 1000);
          }
        });
      } catch (err) {
        //Ad blocker error
        if (typeof Tokenizer === 'undefined') {
          err.message = "Please disable your browser's ad blocker and/or security setting that blocks external files from loading, then refresh this page to continue your order";
        }
        $('.card-errors').show();
        $('.processing').addClass('hide');
        $('#signup [type="submit"]').attr('disabled', false);
        updateSlickHeight($('.form-step.slick-current'));
        console.log(err);
        $('.card-errors').text(err.message);
      }
    }
  });

  var $sections = $('.form-step');

  $sections.each(function (index, section) {
    $(section).find('input').attr('data-parsley-group', 'group-' + index);
    $(section).find('select').attr('data-parsley-group', 'group-' + index);
    $(section).find('textarea').attr('data-parsley-group', 'group-' + index);
  });

  function curIndex() {
    return $sections.index($sections.filter('.slick-current'));
  };

  var myForm = $('#signup').parsley();

  $('.slick-next').on('click', function (e) {
    //boolean to allow carousel to pregoress on success.
    var groupSuccess = myForm.isValid({
      group: 'group-' + curIndex()
    });
    //adds error states to required fields
    myForm.whenValidate({
      group: 'group-' + curIndex()
    });
    if (groupSuccess) {
      $('.carousel-wrap').slick('slickNext');
    } else {
      updateSlickHeight($('.form-step.slick-current'));

    }
  });


  $('.slick-prev').on('click', function (e) {
    $('.carousel-wrap').slick('slickPrev');
  });

  //hide/show nav buttons on first or last slide
  $('.carousel-wrap').on('afterChange', function (event, slick, currentSlide) {
    $('.slick-prev').removeClass('hide-nav');;
    $('.slick-next').removeClass('hide-nav');;
    if (currentSlide == 0) {
      $('.slick-prev').addClass('hide-nav');
    } else if (currentSlide == ((slick.$slides.length) - 1)) {
      $('.slick-next').addClass('hide-nav');
    }
  });

});