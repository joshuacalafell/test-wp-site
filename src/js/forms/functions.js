//Show tax id options when tax id boolean is checked
function taxIdOptions() {
  if ($('[name="tax-id-boolean"]').is(':checked')) {
	$('.tax-id-group').slideDown(function(){
    	updateSlickHeight($('.form-step.slick-current'));
    });
	$('[name="tax-id-pick"]').prop('required', 'required').prop('checked', 'checked');
  }
  else {
    $('.tax-id-group').slideUp(function(){
    	updateSlickHeight($('.form-step.slick-current'));
    });
    $('[name="tax-id-pick"]').removeAttr('required').prop('checked', false); 
  }
}

function hireUsChangeAgent() {
	var pick = $('[name="change-agent"]').val();
	if (pick == 'yes') {
		$('.hire-us-to-change-container').show();
		updateSlickHeight($('.form-step.slick-current'));
		$('.hire-us-to-change-container').hide();
		setTimeout(function(){
			$('.hire-us-to-change-container').slideDown();
		},0);
	}
	else {
		$('.hire-us-to-change-container').slideUp(function(){
			updateSlickHeight($('.form-step.slick-current'));
		});
		$('[name="hire-us-change-agent"]').prop('checked', false);
	}
}

//used to hide/show the Corporation/LLC Sections
function swapEntityType() {
	if ($('[name="signup-type"]').val() === 'Incorporation' || $('[name="signup-type"]').val() === 'Foreign') {
		var type = $('[name="entityType"]').val();
		if (type == 'LLC') {
			//show llc
			$('.e-type-fillin').text('LLC');
			$('.llc').slideDown();
			$('.llc').find('input').attr('required', true);
			$('.llc').find('select').removeAttr('required');

			//hide corp
			$('.corporation').hide();
			$('.corporation').find('input').removeAttr('required');
			$('.corporation').find('select').removeAttr('required');

			//hide np
			$('.np').hide();
			$('.np').find('input').removeAttr('required');
			$('.np').find('select').removeAttr('required');
			updateSlickHeight($('.form-step.slick-current'));
		} else if (type == 'Corp') {
			//show corp
			$('.e-type-fillin').text('Corporation');
			$('.corporation').slideDown();
			$('.corporation').find('input').attr('required', true);
			$('.corporation').find('select').attr('required', true);
			
			//hide llc
			$('.llc').hide();
			$('.llc').find('input').removeAttr('required');
			$('.llc').find('select').removeAttr('required');

			//hide np
			$('.np').hide();
			$('.np').find('input').removeAttr('required');
			$('.np').find('select').removeAttr('required');
			updateSlickHeight($('.form-step.slick-current'));
		} else if (type == 'NP Corp') {
			//show np 
			$('.e-type-fillin').text('Nonprofit');
			$('.np').slideDown();
			$('.np').find('input').attr('required', true);
			$('.np').find('select').attr('required', true);
			$('.np .addName').trigger('click').trigger('click');

			//hide llc 
			$('.llc').hide();
			$('.llc').find('input').removeAttr('required');
			$('.llc').find('select').removeAttr('required');

			//hide corp
			$('.corporation').hide();
			$('.corporation').find('input').removeAttr('required');
			$('.corporation').find('select').removeAttr('required');
			updateSlickHeight($('.form-step.slick-current'));
		}
	}
}

//Hide/show the director input based on the input named officerAreDirectors
//This should be cleaned up
function officersDirectors() {
	if( $('.officers-corporation .person input').first().val() === '' && $('.officers-corporation .person').length === 1) {
		$('.officers-corporation .person').first().hide();
		$('.officers-corporation .person').first().find('.remove-person').show();
		$('.officers-corporation input').first().removeAttr('data-validator').removeAttr('required');
	}
	if ( $('[name="entityType"]').val() === 'Corp' ) {
		if( $('[name="officerAreDirectors"]').val() === 'yes' ) {
			if( $('.directors-corporation .person').first().find('input').first().val() === '' &&  $('.directors-corporation .person').length === 1){
				$('.directors-corporation .person').hide();
			} else {
				$('.director .person').show();
			}
			$('.directors-corporation .person').first().find('.remove-person').show();
			$('.directors-corporation input').removeAttr('data-validator').removeAttr('required');
		} else {
			$('.directors-corporation .person').show();
			$('.directors-corporation .person').first().find('.remove-person').hide();
			$('.directors-corporation input').attr('data-validator', 'corporation').attr('required', true);
		}

		$('.officers-corporation input').removeAttr('data-validator').removeAttr('required');

	} else {
		$('.officers-corporation input').removeAttr('data-validator').removeAttr('required');
		$('.directors-corporation input').removeAttr('data-validator').removeAttr('required');
	}
}

//Hide/show the manager input based on the input named memberOrmanager
//This should be cleaned up
function hideManager() {
	$('.managers .person:first-of-type .remove-person').hide();
	$('.members .person:first-of-type .remove-person').hide();
	if ( $('[name="entityType"]').val() === 'LLC' ) {
		$('.members input').attr('data-validator', 'llc').attr('required', true);
		$('.managers input').attr('data-validator', 'llc').attr('required', true);
		if( $('[name="memberOrmanager"]').val() === 'Member_Managed' ) {
			$('.managers').hide();
			$('.managers input').removeAttr('data-validator').removeAttr('required');
		}
		else {
			$('.managers').show();
		}
	}
	else {
		$('.members input, .members select').removeAttr('data-validator').removeAttr('required');
		$('.managers input, .managers select').removeAttr('data-validator').removeAttr('required');
	}
}

//show attorney email input if notify attorney of lawsuit is true
function showNotifyAttorney() {

	var $container = $('.content');
	//console.log($container);

  if ($('[name="notify-attorney"]').val() == 'yes') {
  	$('.attorney-email').show();
  	updateSlickHeight($container);
  	setTimeout(function(){
  		$('.attorney-email').hide();
    	$('.attorney-email').slideDown();
    	$('[name="attorney-email"]').attr('required', true);
    	$('[name="attorney-email"]').attr('data-parsley-group', 'group-0');
  	}, 0)
  }
  else {
    $('.attorney-email').slideUp().find('input').removeAttr('required').removeAttr('data-parsley-group');
    $('[name="attorney-email"]').removeAttr('required').removeAttr('data-parsley-group');
  }
}

//Show billing address inputs if use different billing is checked
function showBillingAddress() {
  var $heightSetter = $('.billing').parent().parent().parent();
  if ($('[name="differentAddress"]').is(':checked')) {
    $('.billing').slideDown(function(){
    	updateSlickHeight($heightSetter);
    }).find('input').attr('required', true);
		$('[name="billingState"]').attr('required', true);
  }
  else {
    $('.billing').slideUp(function(){
    	updateSlickHeight($heightSetter);
    }).find('input').removeAttr('required');
		//console.log('in');
		$('[name="billingState"]').removeAttr('required');
  }
	//addTokenizerData();
}

//swap mailing inputs based on country
function swapForeignMailingCountry() {
  if ($('[name="mailingCountry"]').val() == 'US') {
    $('.us-mailing').show();
    $('.non-us-mailing').hide();
    $('[name="mailingState"]').attr('required', true);
    $('[name="mailingProvidence"]').removeAttr('required');
  }
  else {
    $('.us-mailing').hide();
    $('.non-us-mailing').show();
    $('[name="mailingState"]').removeAttr('required');
    $('[name="mailingProvidence"]').attr('required', true);
  }
}

//swap billing inputs based on country
function swapForeignBillingCountry() {
	if ($('[name="differentAddress"]').is(':checked')) {
		if ($('[name="billingCountry"]').val() == 'US') {
			$('.us-billing').show();
			$('.non-us-billing').hide();
			$('[name="billingState"]').attr('required', true);
			$('[name="billingProvidence"]').removeAttr('required');
		}
		else {
			$('.us-billing').hide();
			$('.non-us-billing').show();
			$('[name="billingState"]').removeAttr('required');
			$('[name="billingProvidence"]').attr('required', true);
		}
	}
}

//show .password-strength
function showPW() {
  $('.password-strength').slideDown();
}

//check if the password is a valid password (8 characters, mixed case, 1 number, 1 special character)
function passwordStrength() {
	var password = $('[name="password"]').val();
	var isLongEnough = password.length >= 8;
	if(isLongEnough){
		$('.length').removeClass('failure').addClass('success');
	} else {
		$('.length').removeClass('success').addClass('failure');
	}

	var isMixedCase = (password.toUpperCase() !== password) && (password.toLowerCase() !== password);
	if(isMixedCase){
		$('.mixed-case').removeClass('failure').addClass('success');
	} else {
		$('.mixed-case').removeClass('success').addClass('failure');
	}

	var containsNumbers = !!password.match(/\d/);
	if(containsNumbers){
		$('.has-numbers').removeClass('failure').addClass('success');
	} else {
		$('.has-numbers').removeClass('success').addClass('failure');
	}

	var containsSpecialCharacters = !!password.match(/[^a-z0-9]/i);
	if(containsSpecialCharacters){
		$('.has-special').removeClass('failure').addClass('success');
	} else {
		$('.has-special').removeClass('success').addClass('failure');
	}

	var valid = isLongEnough && isMixedCase && containsNumbers && containsSpecialCharacters;
	if(valid == false) {
		return false;
	} else {
		return true;
	}
}

//add the tokenizer data attributes
function addTokenizerData(){
	$('body').find('[data-tokenizer-field="line1"]').removeAttr('data-tokenizer-field');
	$('body').find('[data-tokenizer-field="line2"]').removeAttr('data-tokenizer-field');
	$('body').find('[data-tokenizer-field="city"]').removeAttr('data-tokenizer-field');
	$('body').find('[data-tokenizer-field="state"]').removeAttr('data-tokenizer-field');
	$('body').find('[data-tokenizer-field="zipcode"]').removeAttr('data-tokenizer-field');
	$('body').find('[data-tokenizer-field="country"]').removeAttr('data-tokenizer-field');

	if( $('[name="differentAddress"]').is(':checked') ){
		$('[name="billingCountry"]').attr('data-tokenizer-field', 'country');
		$('[name="billingAddress"]').attr('data-tokenizer-field', 'line1');
		$('[name="billingCity"]').attr('data-tokenizer-field', 'city');
		$('[name="billingZipcode"]').attr('data-tokenizer-field', 'zipcode');
		if( $('[name="billingCountry"]').val() === "US" ){
			$('[name="billingState"]').attr('data-tokenizer-field', 'state');
		} else {
			$('[name="billingProvidence"]').attr('data-tokenizer-field', 'state');
		}
	} else {
		$('[name="mailingCountry"]').attr('data-tokenizer-field', 'country');
		$('[name="mailAddress"]').attr('data-tokenizer-field', 'line1');
		$('[name="mailingCity"]').attr('data-tokenizer-field', 'city');
		$('[name="mailingZipcode"]').attr('data-tokenizer-field', 'zipcode');
		if( $('[name="mailingCountry"]').val() === "US" ){
			$('[name="mailingState"]').attr('data-tokenizer-field', 'state');
		} else {
			$('[name="mailingProvidence"]').attr('data-tokenizer-field', 'state');
		}
	}
}


var baseCost = $('[name="item-cost"]').val();
var grandTotal = 0;
var options = 0;
function totalPrice() {
	var foreignCertFee = 0;
	if ($('[name="signup-type"]').val() === 'Foreign') {
		foreignCertFee = $('[name="cogFee"]').val();
	}

	grandTotal = parseFloat(baseCost) + parseFloat(options) + parseFloat(foreignCertFee);

	var stateFee = $('[name="processing-pick"]').attr('data-price');
	if (stateFee == undefined) {
		stateFee = 0;
	}
	$('.totalPrice').text('$' + grandTotal.toFixed(2));
	$('[name="item-cost"]').val(grandTotal - stateFee);
}

//handle optional items
function optional() {
		options = 0;
		$('.price-footer .input-container .line-item').remove();
		$('.optional').each(function(){
			if( $(this).find('input').first().is(':checked') ) {
				$(this).find('input:checked[data-price]').each(function(){
					if(!$(this).hasClass('ignore') ) {
						var price = parseFloat($(this).attr('data-price') || 0, 10);
						var optionName = $(this).attr('data-name');
						if(price !== null && price !== undefined) {
							options = options + price;
						}
						$('.price-footer .input-container').append('<p class="line-item js-' + optionName + '"><span>' + $(this).attr('data-name') + '</span> <span class="right">$' + price.toFixed(2) + '</span></p>');
					}
				});
			}
		});
}

var alreadyFiltered = false;

function annualReportHandler() {

	

	if($('[name="signup-type"]').val() == "Registered Agent") {

		var inputVal = $('[name="entityType"]').val();

		if (inputVal == 'Corp' || inputVal == 'LLC' || inputVal == 'NP Corp') {
			$('.carousel-wrap').slick('slickUnfilter').slick('refresh');
			//console.log('slide down');
			disableSlickNav()
		    $('.optional-wrap-ra').slideDown();
		    alreadyFiltered = false;

		} else {
			if(!(alreadyFiltered)){
				$('.carousel-wrap').slick('slickFilter', '.not-optional').slick('refresh');
				disableSlickNav();
				//console.log('slide up');
				$('.optional-wrap-ra').slideUp();
				alreadyFiltered = true;
			}
		}
	}
}

function handleTaxId() {
  if ($('[name="tax-id-boolean"]').is(':checked')) {
    $('.tax-id-group').slideDown();
    updateSlickHeight($('.form-step.slick-current'));
  }
  else {
    $('.tax-id-group').slideUp(function(){
    	updateSlickHeight($('.form-step.slick-current'));
    });
    $('[name="tax-id-type"]').prop('checked', false);
  }
}
$('[name="tax-id-boolean"]').on('click', function(){
  handleTaxId();
  optional();
  totalPrice();
});

function disableSlickNav(){
	$('.slick-dots li button').on('click', function(e) {
        e.stopPropagation(); 
    });
};

function updateSlickHeight(heightObj){
	var containerHeight = heightObj.outerHeight();
    $('.slick-list').css('height', (containerHeight)+'px');
};


//call everything on start
function initialSetup() {
	$('.carousel-wrap').slick({
        adaptiveHeight: true,
        dots: false,
        infinite: false,
        draggable: false,
        arrows: false,
        swipe: false
    });
	addTokenizerData();
	showBillingAddress();
	swapForeignBillingCountry();
	swapEntityType();
	officersDirectors();
	hideManager();
	taxIdOptions();
}
