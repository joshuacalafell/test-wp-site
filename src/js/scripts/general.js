$(function () {
    //Do yo JAHVAHSCRIPT here.

    //mobile nav controller
    $('.mobile-nav-toggle').on('click', function () {
        $(this).toggleClass('active');
        $('.nav').toggleClass('mobile-active');
        $('.top-bar').toggleClass('mobile-active');
    });

    //just uncomment which style of help text you would like to use for your site

    //********TOGGLE STYLE********//

    // Help Text toggle - tool tip style

    $('.help-text-icon').on('click', function (e) {
        e.stopPropagation();
        var $clicked = $(this);

        if (!($clicked.hasClass('current'))) {
            $('.help-text-icon').removeClass('current');
            $('.help-text-container').hide();
            $clicked.addClass('current');
            $clicked.find('.help-text-container').show();
        } else {
            $('.help-text-icon').removeClass('current');
            $('.help-text-container').hide();
        }

    });

    $('.help-text-container').on('click', function (e) {
        e.stopPropagation();
        $(this).hide();
        $('.help-text-icon').removeClass('current');
        $('.help-text-container').hide();
    });

    $(window).on('click', function () {
        $('.help-text-icon').removeClass('current');
        $('.help-text-container').hide();
    });
    //********TOGGLE STYLE END********//


    //********MODAL STYLE********//

    // $('.help-text-icon').on('click', function(e) {
    //     e.stopPropagation();
    //     var $clicked = $(this);
    //     var helpText = $clicked.find('.help-text-container').html();
    //     console.log(helpText);
    //     $('.help-text-modal .inner').html(helpText);
    //     $('.help-text-modal').removeClass('hidden');
    // });


    // $('.help-text-modal .close').on('click', function(){
    //     $('.help-text-modal').addClass('hidden');
    // });


    // $('.help-text-modal').on('click', function(e){
    //     e.stopPropagation();
    //     $('.help-text-modal').addClass('hidden');
    // });

    //********MODAL STYLE END********//
});