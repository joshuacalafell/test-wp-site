			<footer class="footer" role="contentinfo">
				<p class="copyright">
					&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>
				</p>
			</footer>
		</div>
		<!-- /wrapper -->
		<?php wp_footer(); ?>
		<script id="__bs_script__">//<![CDATA[
		document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.13'><\/script>".replace("HOST", location.hostname));
	//]]></script>
	</body>
</html>
