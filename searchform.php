<?php
	$searchValue = '';
	if(isset($_GET['s'])) {
		$searchValue = $_GET['s'];
	}
?>
<form class="search" autocomplete="off" method="get" action="<?php echo home_url(); ?>" role="search">
	<div>
		<input class="search-input" required type="search" name="s" value="<?=$searchValue?>" placeholder="Search...">
		<button class="search-submit" type="submit" role="button"><i class="fa fa-search" aria-hidden="true"></i></button>
	</div>
</form>