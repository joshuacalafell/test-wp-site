/**
 *   COMMON COMMANDS
 * 
 *   gulp           Builds all files (CSS, JS, IMGs)
 *   gulp build     Builds all files (CSS, JS, IMGs)
 *   gulp styles    Builds all CSS
 *   gulp scripts   Builds all JS
 *   gulp img       Builds all IMGs
 *   gulp clean     Deletes assets folders
 *   gulp watch     Starts browserSync (Hot reloading via localhost:3000)
 */

//Setup
'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var cache = require('gulp-cache');
var browserSync = require('browser-sync').create();
var del = require('del');
var babel = require('gulp-babel');
var util = require('gulp-util');
var stylish = require('jshint-stylish');
var plumber = require('gulp-plumber');
var csso = require('gulp-csso');
sass.compiler = require('node-sass');

//Config
var devUrl = "http://test-wp-site:8888"; // UPDATE THIS TO YOUR PROJECTS DEV SERVER URL
var jslibs = require('./src/js/scripts.json');
var paths = {
    styles: {
        src: 'src/css/scss/style.scss', //css entry point 
        dest: 'assets/css/' //css exit point
    },
    scripts: {
        src: 'src/js/scripts/*.js', //js entry point
        dest: 'assets/js/scripts/' //js exit point
    },
    formScripts: {
        src: 'src/js/forms/*.js', //forms js entry point
        dest: 'assets/js/forms/' //forms js exit point
    },
    images: {
        src: 'src/img/*', //img entry point
        dest: 'assets/img' //img exit point
    }
};


//TASKS
//build / watch / imgs / clean / misc tasks
gulp.task('default', ['styles', 'libs-js', 'scripts', 'form-scripts', 'images', 'watch']);

gulp.task('build', ['styles', 'libs-js', 'scripts', 'form-scripts', 'images', 'watch']);

gulp.task('clean', function () {
    return del(['assets/css', 'assets/js', 'assets/img']);
});

gulp.task('images', function () {
    return gulp.src(paths.images.src)
        .pipe(errorHandler())
        .pipe(cache(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(paths.images.dest))
        .pipe(notify({
            message: 'Images task complete'
        }));
});

gulp.task('watch', function () {
    //start browserSync
    browserSync.init({
        proxy: devUrl
    });

    //files/folders to watch
    gulp.watch('**/*.php').on('change', browserSync.reload);
    gulp.watch('**/*.html').on('change', browserSync.reload);
    gulp.watch('src/css/**/*.scss', ['styles']).on('change', browserSync.reload);
    gulp.watch('src/js/scripts/*.js', ['scripts']).on('change', browserSync.reload);
    gulp.watch('src/js/forms/*.js', ['form-scripts']).on('change', browserSync.reload);
    gulp.watch('src/js/scripts.json', ['libs-js']).on('change', browserSync.reload);
    gulp.watch('src/img/**/*', ['images']).on('change', browserSync.reload);
});

//Libs related tasks
gulp.task('libs-js', function () {
    var srcs = [];
    for (var jl in jslibs) {
        srcs.push(jslibs[jl]);
    }
    mvFilesConcat(srcs, paths.scripts.dest, 'libs.js');
});

//Helper Functions / Misc
function mvFilesConcat(src, dest, concatName) {
    return gulp.src(src)
        .pipe(concat(concatName))
        .pipe(gulp.dest(dest));
}

// error handler to prevent watch task from crashing //https://andidittrich.de/2016/03/prevent-errors-from-breaking-gulp-watch.html
function errorHandler() {
    return plumber(function (error) {
        util.log('|- ' + util.colors.bgRed.bold('Build Error in ' + error.plugin));
        util.log('|- ' + util.colors.bgRed.bold(error.message));
        util.log('|- ' + util.colors.bgRed('>>>'));
        util.log('|\n    ' + msg + '\n           |');
        util.log('|- ' + util.colors.bgRed('<<<'));
    });
}



//CSS tasks
gulp.task('styles', function () {
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(csso({
            sourceMap: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(notify({
            message: 'Styles task complete'
        }))
        .pipe(browserSync.stream({
            match: '**/*.css'
        }));
});



//JS tasks
gulp.task('scripts', function () {
    return gulp.src(paths.scripts.src)
        .pipe(errorHandler())
        // .pipe(jshint())
        // .pipe(jshint.reporter(stylish))
        .pipe(babel())
        .pipe(sourcemaps.init())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(notify({
            message: 'Scripts Task Complete!'
        }));
});

gulp.task('form-scripts', function () {
    return gulp.src(paths.formScripts.src)
        .pipe(errorHandler())
        // .pipe(jshint())
        // .pipe(jshint.reporter(stylish))
        .pipe(babel())
        .pipe(concat('forms.js'))
        .pipe(sourcemaps.init())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write(''))
        .pipe(gulp.dest(paths.formScripts.dest))
        .pipe(notify({
            message: 'Form-scripts Task Complete!'
        }));
});
