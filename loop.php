<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<article class="search-result">
		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
		<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
		<?php edit_post_link(); ?>
	</article>
<?php endwhile; ?>
<?php else: ?>
	<article>
		<h2><?php _e( 'No results found.', 'html5blank' ); ?></h2>
	</article>
<?php endif; ?>
