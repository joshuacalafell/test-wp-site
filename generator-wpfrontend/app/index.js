var generators = require('yeoman-generator');

module.exports = generators.Base.extend({
  /* Figure out what we're doing from the user */
	prompting: function() {
    var done = this.async();
    var that = this;
    this.prompt([{
      type    : 'list',
      name    : 'direction',
      message : 'Add a New Page or a New Module?',
      choices: [
        {name:"New Page", value:"page"},
        {name:"New Module", value:"module"}
      ]
    },{
      when    : function(answers){ return answers.direction == "page"; },
      type    : 'input',
      name    : 'name',
      message : 'Name your page',
    },{
      when    : function(answers){ return answers.direction == "module"; },
      type    : 'input',
      name    : 'name',
      message : 'Name your module',
    }], function (answers) {
      this.answers = answers;
      done();
    }.bind(this));
	},
  /* generate the stuff! */
  writing: function () {
    if(this.answers.direction == "page") {
        this.template('page/page.php', 'page-templates/'+this.answers.name+'.php');
    }
    if(this.answers.direction == "module") {
        this.template('modules/module.php', 'template-parts/'+this.answers.name+'.php');
        this.template('modules/module.scss', 'src/css/scss/modules/'+this.answers.name+'.scss');
    }
  }

});