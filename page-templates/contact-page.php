<?php
/**
** Template Name: FORM - Contact 
**/
session_start();
include (TEMPLATEPATH . '/forms/processing-bits/form-bits.php');
include (TEMPLATEPATH . '/forms/processing-bits/functions.php');
get_header(); 
?>

<main role="main">
	<section>
		<h1><?php the_title(); ?></h1>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<article>
			<?php if(isset($_SESSION['contactErrors'])): ?>
			<div class="errors">
				<?php
				//if there are errors echo them out
				printable_error($_SESSION['contactErrors']);
				unset($_SESSION['contactErrors']);
				?>
			</div>
			<?php endif; ?>
			<form data-parsley-validate="" method="post" action="<?php echo get_template_directory_uri(); ?>/forms/contact.php" id="contact-form">
				<div class="label-group">
					<label for="name">Name</label>
				</div>
				<div class="input-group">
					<input id="name" name="name" type="text">
				</div>
				<div class="label-group">
					<label for="email">Email</label>
				</div>
				<div class="input-group">
					<input id="email" name="email" type="text" required>
				</div>
				<div class="label-group">
					<label for="message">Message</label>
				</div>
				<div class="input-group">
					<textarea id="message" name="message" type="text" required></textarea>
				</div>
				<div style="display:none;">
					<input id="fake-input" type="text" placeholder="Leave blank" name="fake-input">
				</div>
				<div class="text-center">
					<button type="submit">Send Message</button>
				</div>
			</form>
		</article>
		<?php endwhile; endif; ?>
	</section>
</main>
<script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.8.1/dist/parsley.min.js"></script>
<?php get_footer(); ?>