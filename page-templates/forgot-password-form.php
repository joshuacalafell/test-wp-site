<?php
/**
** Template Name: FORM - Password Reset Form
**/
session_start();
include (TEMPLATEPATH . '/forms/processing-bits/form-bits.php');
include (TEMPLATEPATH . '/forms/processing-bits/functions.php');
include (TEMPLATEPATH . '/forms/processing/inc/website-info.php');
get_header(); 
?>

<main role="main">
	<section>
		<h1><?php the_title(); ?></h1>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php if(isset($_SESSION['forgotSuccess'])): ?>
			<div class="errors success">
				<ul>
					<?php
					printable_error($_SESSION['forgotSuccess']);
					unset($_SESSION['forgotSuccess']);
					?>
				</ul>
			</div>
			<?php endif; ?>
			<form data-parsley-validate="" method="post" action="<?=get_bloginfo('template_directory');?>/forms/processing/forgot.php" id="signin-form">
				<div class="label-group">
					<label for="username">Email</label>
				</div>
				<div class="input-group">
					<input id="username" name="username" type="email" required>
				</div>
				<div class="text-center">
					<button type="submit">Submit</button>
				</div>
				<a href="<?php echo LOGIN_PAGE;?>">Login</a>
			</form>
		</article>
		<?php endwhile; endif; ?>
	</section>
</main>
<script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.8.1/dist/parsley.min.js"></script>
<?php get_footer(); ?>