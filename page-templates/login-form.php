<?php
/**
** Template Name: FORM - Login
**/
session_start();
include (TEMPLATEPATH . '/forms/processing-bits/form-bits.php');
include (TEMPLATEPATH . '/forms/processing-bits/functions.php');
include (TEMPLATEPATH . '/forms/processing/inc/website-info.php');
get_header(); 
?>

<main role="main">
	<section>
		<h1><?php the_title(); ?></h1>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<article>
			<?php if(isset($_SESSION['loginErrors'])): ?>
			<div class="errors">
				<?php
				//if there are errors echo them out
				echo '<ul>';
				printable_error($_SESSION['loginErrors']);
				unset($_SESSION['loginErrors']);
				echo '</ul>';
				?>
			</div>
			<?php endif; ?>
			<form data-parsley-validate="" method="post" action="<?php echo get_template_directory_uri(); ?>/forms/processing/login.php" id="signin-form">
				<div class="label-group">
					<label for="username">Email</label>
				</div>
				<div class="input-group">
					<input id="username" name="username" type="email" required>
				</div>
				<div class="label-group">
					<label for="password">Password</label>
				</div>
				<div class="input-group">
					<input id="password" name="password" type="password" required>
				</div>
				<div class="text-center">
					<button type="submit">Login</button>
				</div>
				<a href="<?php echo FORGOT_PASSWORD_PAGE;?>">Forgot My Password</a>
			</form>
		</article>
		<?php endwhile; endif; ?>
	</section>
</main>
<script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.8.1/dist/parsley.min.js"></script>
<?php get_footer(); ?>