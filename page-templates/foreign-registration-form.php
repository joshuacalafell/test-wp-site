<?php
/**
** Template Name: FORM - Order Foreign Registration
**/
session_start();
get_header();
include (TEMPLATEPATH . '/forms/processing-bits/form-bits.php');
include (TEMPLATEPATH . '/forms/processing-bits/functions.php');
include (TEMPLATEPATH . '/forms/processing/inc/website-info.php');
?>
<main role="main">
	<section>
		<h1><?php the_title(); ?></h1>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<article>
			<?php if(isset($_SESSION['errors'])): ?>
			<div class="errors">
				<ul>
					<?php
						printable_error($_SESSION['errors']);
						unset($_SESSION['errors']);
					?>
				</ul>
			</div>
			<?php endif; ?>
			<form id="signup" data-parsley-validate="" method="post" action="<?=get_bloginfo('template_directory');?>/forms/processing/index.php">
				<div class="step">
					<div class="content">
						<h3 class="form-header">Company Information</h3>
						<div class="label-group">
							<label>Company Name</label>
						</div>
						<div class="input-group">
							<input required type="text" name="companyName">
						</div>
						<div class="label-group">
							<label>Where Would You Like to Register your Business In?</label>
						</div>
						<div class="input-group">
							<select name="service-j">
								<?=stateOnlyList('service-j');?>
							</select>
						</div>
                        <div class="processing-opts">
							<div class="label-group">
								<label>Processing Time</label>
							</div>
							<div class="input-group">
							</div>
						</div>
                        <div class="label-group">
							<label>What is the Home State of your Business?</label>
						</div>
						<div class="input-group">
							<select name="home-state">
								<?=stateOnlyList('home-state');?>
							</select>
						</div>
						<div class="half-group first">
							<div class="label-group">
								<label>Type of Business</label>
							</div>
							<div class="input-group">
								<select style="width:100%;" required name="entityType">
									<option value="LLC">LLC</option>
									<option value="Corp">Corporation</option>
									<option value="NP Corp">Nonprofit Corporation</option>
								</select>
							</div>
						</div>
						<div class="half-group last">
							<div class="label-group">
								<label>Notify Your Attorney of a Lawsuit</label>
							</div>
							<div class="input-group">
								<select required name="notify-attorney">
									<option value="no">No</option>
									<option value="yes">Yes</option>
								</select>
							</div>
						</div>
						<div class="attorney-email">
							<div class="label-group">
								<label>Attorney Email:</label>
							</div>
							<div class="input-group">
								<input name="attorney-email" type="email" autocapitalize="off" autocorrect="off">
							</div>
						</div>
						<div class="label-group">
							<label>Business Purpose</label>
						</div>
						<div class="input-group">
							<textarea required type="text" name="b-purpose"></textarea>
						</div>
					</div>
				</div>

				<?php include(TEMPLATEPATH . '/forms/html/form-company-html.php'); ?>
				<?php include(TEMPLATEPATH . '/forms/html/contact-html.php'); ?>
				<?php include(TEMPLATEPATH . '/forms/html/account-html.php'); ?>
				<?php include(TEMPLATEPATH . '/forms/html/optional-html.php'); ?>
				<?php include(TEMPLATEPATH . '/forms/html/payment-html.php'); ?>

				<div class="footer-funnel">
					<div class="price-footer">
						<h3>Order Summary</h3>
						<div class="input-container the-line-items">
							<p><span>Registered Agent Service </span><span class="right">$100.00</span></p>
							<p><span>Business Registration Service Fee</span><span class="right">$100.00</span></p>
							<p class="cert-fee"><span>Certificate Fee </span><span class="right">$0.00</span></p>
						</div>
						<p class="total"><span>Total </span><span class="right totalPrice">$200.00</span></p>
					</div>
				</div>

				<div class="terms">
					<label>
						<span>By clicking submit, I agree to the <a href="<?php echo TERMS_PAGE;?>">terms of service.</a></span>
					</label>
				</div>

				<div class="text-center">
					<button class="button" type="submit">Submit Order</button>
				</div>

				<span class="card-errors"></span>

				<div class="processing hide">
					<div class="processing-content">
						<i class="fas fa-spinner fa-spin fa-3x fa-fw text-danger"></i>
						Processing &ndash; please wait.<br>DO NOT refresh the page.
					</div>
				</div>

				<div class="hidden-inputs" style="display:none;">
					<input type="hidden" name="item-cost" value="200">
					<input type="hidden" name="state-cost" value="0">
					<input type="hidden" name="cogFee" value="0">
					<input type="hidden" name="signup-type" value="Foreign">
				</div>

			</form>
		</article>
		<?php endwhile; ?>
		<?php endif; ?>
	</section>
	<div class="help-text-modal hidden"><div class="inner-wrap"><div class="inner"></div><div class="close"><i class="fas fa-times"></i></div></div></div>
</main>
<script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.8.1/dist/parsley.min.js"></script>
<script async src="https://static.onlineaccount.net/assets/tokenizer.js"></script>
<script async src="https://static.onlineaccount.net/assets/global.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/forms/forms.min.js"></script>
<?php get_footer(); ?>