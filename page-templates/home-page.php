<?php
/**
 * Template Name: Home Page
 */
?>
<?php get_header(); ?>
	<main role="main">
		<section>
			<!-- <h1><//?php the_title(); ?></h1> -->
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<article>
					<h1>HOME PAGE STUFF HERE</h1>
					<img class="cat-gif" src="<?php echo get_template_directory_uri();?>/assets/img/cat2.gif" alt="Cat Gif" Title="Cat Gif">
				</article>
			<?php endwhile; ?>
			<?php endif; ?>
		</section>
	</main>
<?php get_footer(); ?>
